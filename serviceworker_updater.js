if ('serviceWorker' in navigator)
	window.addEventListener('load', async () => 
	{
		const registration = await navigator.serviceWorker.register('/serviceworker.js')

		if (registration.waiting)
			registration.waiting.postMessage('skipWaiting')

		registration.addEventListener('updatefound', () => 
		{
			if (registration.installing)
				registration.installing.addEventListener('statechange', () => 
				{
					if (registration.waiting)
						if (navigator.serviceWorker.controller)
							registration.waiting.postMessage('skipWaiting')
						else
							caches.keys()
				})

			let refreshing = false

			navigator.serviceWorker.addEventListener('controllerchange', () => 
			{
				if (!refreshing) 
				{
					window.location.reload()
					refreshing = true
				}
			})
		})
	})