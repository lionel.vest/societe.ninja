async function sirene(siren)
{

	if (!get_cookie('2'))
		t = await fetch('t.php')

	let info = { "siren": siren, "source": "SIRENE V3 (Unité légale)", "sourcelink": "https://portail-api.insee.fr", "retry": () => sirene(siren) }
	add_source(info.source, info.sourcelink, 15)

	fetch('https://api.insee.fr/api-sirene/3.11/siren/' + siren,
		{
			headers:
			{
				'accept': 'application/json',
				"X-INSEE-Api-Key-Integration": atob(get_cookie('2'))
			}
		})
		.then(function (response) 
		{
			if (response.ok)
				return response.json()
			else
				throw response
		})
		.then(function (response)
		{
			response = Object.assign(response, info)

			if (!response.uniteLegale)
				return update_source(response.source, 'validate')

			if (response.uniteLegale.statutDiffusionUniteLegale == 'N')
				return update_source(response.source, 'validate', '403', "Accès refusé\nL'entrepreneur individuel s'est opposé à la diffusion de ses données")

			denomination = new Array()
			if (response.uniteLegale.prenom1UniteLegale) denomination.push(response.uniteLegale.prenom1UniteLegale)
			if (response.uniteLegale.prenom2UniteLegale) denomination.push(response.uniteLegale.prenom2UniteLegale)
			if (response.uniteLegale.prenom3UniteLegale) denomination.push(response.uniteLegale.prenom3UniteLegale)
			if (response.uniteLegale.prenom4UniteLegale) denomination.push(response.uniteLegale.prenom4UniteLegale)
			if (response.uniteLegale.periodesUniteLegale[0].nomUniteLegale) denomination.push(response.uniteLegale.periodesUniteLegale[0].nomUniteLegale)
			if (response.uniteLegale.nom_usage) denomination.push(response.uniteLegale.nom_usage)
			if (response.uniteLegale.periodesUniteLegale[0].denominationUniteLegale) denomination.push(response.uniteLegale.periodesUniteLegale[0].denominationUniteLegale)

			document.title = siren + ' (' + denomination.join(' ') + ')'
			document.getElementById('company_name').innerHTML = denomination.join(' ').toUpperCase()

			document.getElementById('menu_unite_legale').style.display = ''
			document.getElementById('unite_legale').style.display = ''

			document.getElementById('menu_documents').style.display = ''
			document.getElementById('documents').style.display = ''

			documents_addcell('Avis<br/>SIRENE', function () { window.open('https://api.avis-situation-sirene.insee.fr/identification/pdf/' + response.uniteLegale.siren + response.uniteLegale.periodesUniteLegale[0].nicSiegeUniteLegale) })
			documents_addcell('Export<br/>SOCIETE.NINJA', function () { window.print() })

			nom_commercial = new Array()
			if (response.uniteLegale.periodesUniteLegale[0].denominationUsuelle1UniteLegale) nom_commercial.push(response.uniteLegale.periodesUniteLegale[0].denominationUsuelle1UniteLegale)
			if (response.uniteLegale.periodesUniteLegale[0].denominationUsuelle2UniteLegale) nom_commercial.push(response.uniteLegale.periodesUniteLegale[0].denominationUsuelle2UniteLegale)
			if (response.uniteLegale.periodesUniteLegale[0].denominationUsuelle3UniteLegale) nom_commercial.push(response.uniteLegale.periodesUniteLegale[0].denominationUsuelle3UniteLegale)

			unite_legale_addrow("Dénomination Sociale", denomination.join(' '), response.source, 10)
			if (nom_commercial.length > 0)
				unite_legale_addrow("Nom Commercial", nom_commercial.join('<br/>'), response.source, 25)
			unite_legale_addrow("Forme Juridique", categorie_juridique[response.uniteLegale.periodesUniteLegale[0].categorieJuridiqueUniteLegale], response.source, 30)
			unite_legale_addrow("Immatriculation", new Date(response.uniteLegale.dateCreationUniteLegale).toLocaleDateString(), response.source, 50)
			unite_legale_addrow("N° SIREN", siren, response.source, 70)
			unite_legale_addrow("Code NAF", response.uniteLegale.periodesUniteLegale[0].activitePrincipaleUniteLegale + ' <i>(' + response.uniteLegale.periodesUniteLegale[0].nomenclatureActivitePrincipaleUniteLegale + ')</i>', response.source, 80)
			unite_legale_addrow("Activité", naf[response.uniteLegale.periodesUniteLegale[0].nomenclatureActivitePrincipaleUniteLegale + '/' + response.uniteLegale.periodesUniteLegale[0].activitePrincipaleUniteLegale], response.source, 90)
			unite_legale_addrow("Effectif salarié", response.uniteLegale.trancheEffectifsUniteLegale ? effectif_salarie[response.uniteLegale.trancheEffectifsUniteLegale] : 'Inconnu', response.source, 100)
			unite_legale_addrow("TVA Intracommunautaire", "FR" + ((((siren % 97) * 3) + 12) % 97) + "" + siren, response.source, 110)

			idcc(query.get('siren') + response.uniteLegale.periodesUniteLegale[0].nicSiegeUniteLegale)

			update_source(response.source, 'validate', response)
		})
		.catch(function (response)
		{
			response = Object.assign(response, info)
			if (response.toString().substring(0, 9) == 'TypeError')
				return update_source(response.source, 'refresh', '502', response, response.retry)
			else if (response.status == 400)
				return update_source(response.source, 'refresh', '400', "Requête mal formatée", response.retry)
			else if (response.status == 401)
				return update_source(response.source, 'refresh', '401', "Accès non autorisé", response.retry)
			else if (response.status == 404)
				return update_source(response.source, 'validate')
			else if (response.status == 403)
				return update_source(response.source, 'refresh', '403', "Accès refusé", response.retry)
			else if (response.status)
				return update_source(response.source, 'refresh', response.status, response.status + " " + response.statusText, response.retry)
			else
				return update_source(response.source, 'refresh', '???', response, response.retry)
		})
}

function documents_addcell(description, action)
{
	if (!document.getElementById('documents').tBodies[0].rows[0])
		document.getElementById('documents').tBodies[0].insertRow()

	td = document.getElementById('documents').tBodies[0].rows[0].insertCell()
	td.style.textAlign = 'center'
	td.style.width = '500px'
	td.style.cursor = 'pointer'
	td.innerHTML = '<img src="/images/pdf.svg" alt="document" style="max-height:48px;max-width:48px"/><br/>' + description
	td.onclick = action
}



function readable_address(etablissement)
{
	ligne1 = [etablissement.numero_voie, etablissement.indice_repetition, etablissement.type_voie, etablissement.libelle_voie].filter(Boolean).join(' ')
	ligne4 = [etablissement.code_cedex, etablissement.libelle_cedex].filter(Boolean).join(' ')
	ligne5 = [etablissement.code_postal, etablissement.libelle_commune, etablissement.libelle_commune_etranger].filter(Boolean).join(' ')
	return [ligne1, etablissement.complement_adresse, etablissement.distribution_speciale, ligne4, ligne5, etablissement.libelle_pays_etranger]
}