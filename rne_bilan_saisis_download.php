<?php
$curl = curl_init();

curl_setopt($curl, CURLOPT_URL, "https://registre-national-entreprises.inpi.fr/api/bilans-saisis/" . $_GET['id']);
//curl_setopt($curl, CURLOPT_POSTFIELDS, "grant_type=client_credentials&client_id=" . $piste_client_id . "&client_secret=" . $piste_secret . "&scope=openid");
curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . file_get_contents('private/inpi')));
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HEADER, 0);
$result = curl_exec($curl);

curl_close($curl);


//if (sizeof($result)==0 && $http_status==200)
	//die("<html><body style=\"font:normal 14px Consolas\"><p>Ce fichier n'a pas pu être trouvé dans la base publique<br/>Certains dépôts récents peuvent ne pas encore avoir été mis en ligne<br/>Certains documents comme les déclarations de bénéficiaires effectifs peuvent également être soumis à une règle de confidentialité qui interdit leur communication</p></body></html>");

if ($_GET['method']=='inline')
{
	header('Content-type: application/json');
	header('Content-Disposition: inline; filename="' . $_GET['id'] . '.json"');
}
else
{
	header('Content-Description: File Transfer');
	header('Content-Type: application/force-download');
	header('Content-Length: ' . strlen($result));
	header('Content-Disposition: attachment; filename="' . $_GET['id'] . '.json"');
}

echo $result;
include('config.php');
if ($debug == 1)
	mysqli_query($connection, 'INSERT INTO logs SET execution_time = "' . date('Y-m-d H:i:s') . '", ip = "' . $_SERVER['REMOTE_ADDR'] . '", operation = "bilan-saisi_by_id", detail = "' . mysqli_real_escape_string($connection, $_GET['id']) . '", filesize = "' . strlen($result) . '"');
?>
