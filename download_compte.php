<?php
header("X-Robots-Tag: noindex, nofollow", true);

if ($_GET['siren'] && !preg_match('/^[0-9]{9}+$/',$_GET['siren'])) die ('Invalid siren : ' . $_GET['siren']);
if ($_GET['cloture'] && !preg_match('/^[0-9]{8}+$/',$_GET['cloture'])) die ('Invalid cloture : ' . $_GET['cloture']);

include('config.php');

$curl = curl_init();

$jsessionid = file_get_contents('private/imr');

if ($_GET['filetype']=='pdf')
	curl_setopt($curl, CURLOPT_URL, "https://opendata-rncs.inpi.fr/services/diffusion/bilans/find?siren=" . $_GET['siren'] . "&dateClotureDebut=" . $_GET['cloture'] . "&dateClotureFin=" . $_GET['cloture']);
else if ($_GET['filetype']=='xml')
	curl_setopt($curl, CURLOPT_URL, "https://opendata-rncs.inpi.fr/services/diffusion/bilans-saisis/find?siren=" . $_GET['siren'] . "&dateClotureDebut=" . $_GET['cloture'] . "&dateClotureFin=" . $_GET['cloture']);

curl_setopt($curl, CURLOPT_HTTPHEADER, array('Cookie: JSESSIONID='.$jsessionid));
curl_setopt($curl, CURLOPT_POST, 0);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 20);
curl_setopt($curl, CURLOPT_HEADER, false);
$result = curl_exec($curl);

$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
if ($http_status!=200)
	die("ERREUR " . $http_status . " <br/>L'API RCS est momentanément indisponible<br/>Veuillez réessayer ultérieurement");

$response = json_decode($result);

//if (!is_array($results))
	//die("ERREUR " . $http_status . " <br/>L'API RCS a retourné une réponse invalide<br/>Veuillez réessayer ultérieurement");

$_GET['id_fichier'] = $response[0]->idFichier;

curl_setopt($curl, CURLOPT_URL, "https://opendata-rncs.inpi.fr/services/diffusion/document/get?listeIdFichier=" . $_GET['id_fichier']);
curl_setopt($curl, CURLOPT_HTTPHEADER, array('Cookie: JSESSIONID='.$jsessionid));
curl_setopt($curl, CURLOPT_BINARYTRANSFER, 1);
curl_setopt($curl, CURLOPT_POST, 0);
curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 20);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$zip = curl_exec($curl);

if (strpos($zip, 'votre quota quotidien de volume de données téléchargeables sera dépassé'))
	die("ERREUR : le site a dépassé son quota quotidien de téléchargement auprès de l'INPI. Merci de ré-essayer après minuit");

$filesectors = explode("\x50\x4b\x01\x02", $zip);
$filesectors = $filesectors[0] . "\x50\x4b\x01\x02" . $filesectors[1];
$filesectors = explode("\x50\x4b\x03\x04", $filesectors);
$zip_in_zip = $filesectors[1] . "\x50\x4b\x03\x04" . $filesectors[2];
$filedescription = unpack("vversion/vflag/vmethod/vmodification_time/vmodification_date/Vcrc/Vcompressed_size/Vuncompressed_size/vfilename_length/vextrafield_length", $zip_in_zip);
$zip = gzinflate(substr($zip_in_zip,26+$filedescription['filename_length'],-12));

$filesectors = explode("\x50\x4b\x01\x02", $zip);
$filesectors = explode("\x50\x4b\x03\x04", $filesectors[0]);
array_shift($filesectors);
foreach($filesectors as $filesector)
{
	$filedescription = unpack("vversion/vflag/vmethod/vmodification_time/vmodification_date/Vcrc/Vcompressed_size/Vuncompressed_size/vfilename_length/vextrafield_length", $filesector);
	$filedescription['filename'] = substr($filesector,26,$filedescription['filename_length']);
	if (substr($filedescription['filename'],-3) == $_GET['filetype'])
		$file = gzinflate(substr($filesector,26+$filedescription['filename_length'],-12));
}

curl_close($curl);

if (sizeof($file)==0 && $http_status==200)
	die("<html><body style=\"font:normal 14px Consolas\"><p>Ce fichier n'a pas pu être trouvé dans la base publique<br/>Certains dépôts récents peuvent ne pas encore avoir été mis en ligne<br/>Certains documents comme les déclarations de bénéficiaires effectifs peuvent également être soumis à une règle de confidentialité qui interdit leur communication</p></body></html>");

if ($_GET['method']=='inline')
{
	header('Content-type: application/' . $_GET['filetype']);
	header('Content-Disposition: inline; filename="' . $_GET['id_fichier'] . "." .$_GET['filetype'] . '"');
}
else
{
	header('Content-Description: File Transfer');
	header('Content-Type: application/force-download');
	header('Content-Length: ' . strlen($file));
	header('Content-Disposition: attachment; filename="' . $_GET['id_fichier'] . "." .$_GET['filetype'] . '"');
}

echo $file;

include('config.php');
if ($debug == 1)
	mysqli_query($connection, 'INSERT INTO logs SET execution_time = "' . date('Y-m-d H:i:s') . '", ip = "' . $_SERVER['REMOTE_ADDR'] . '", operation = "bilan_by_id", detail = "' . mysqli_real_escape_string($connection, $_GET['id_fichier']) . '", filesize = "' . strlen($zip) . '"');
?>
