<?php
if ($_GET['siren'] && !preg_match('/^[0-9]{9}+$/',$_GET['siren'])) die ('Invalid siren : ' . $_GET['siren']);
$jsessionid = file_get_contents('imr_token.txt');

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, "https://opendata-rncs.inpi.fr/services/diffusion/actes/find?siren=" . $_GET['siren']);
curl_setopt($curl, CURLOPT_HTTPHEADER, array('Cookie: JSESSIONID='.$jsessionid));
curl_setopt($curl, CURLOPT_POST, 0);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HEADER, false);
$result = curl_exec($curl);
$response_actes = json_decode($result);

http_response_code(200);
echo json_encode(array("code" => 200, "data" => $response_actes));
?>