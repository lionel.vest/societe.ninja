<?php
include('config.php');

$curl = curl_init();

$token = getallheaders()['Token'];
if ($token != 'null')
{
	$query = mysqli_query($connection, "SELECT id FROM rbe_users WHERE `token` = '" . $token . "'");
	if (mysqli_num_rows($query) > 0)
	{
		$result = mysqli_fetch_array($query, MYSQLI_ASSOC);
		mysqli_query($connection, 'INSERT INTO logs SET execution_time = "' . date('Y-m-d H:i:s') . '", ip = "' . $_SERVER['REMOTE_ADDR'] . '", operation = "rbe", detail = "' . mysqli_real_escape_string($connection, $_GET['siren']) . '", uid = "' . $result['id'] . '"');
		$authorized = 1;
	}
}

curl_setopt($curl, CURLOPT_URL, "https://registre-national-entreprises.inpi.fr/api/companies/" . $_GET['siren']);
curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . ($authorized == 1 ? file_get_contents('private/rbe') : file_get_contents('private/inpi'))));
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HEADER, 0);
$result = curl_exec($curl);
echo $result;

if ($debug == 1)
	mysqli_query($connection, 'INSERT INTO logs SET execution_time = "' . date('Y-m-d H:i:s') . '", ip = "' . $_SERVER['REMOTE_ADDR'] . '", operation = "imr_by_siren", detail = "' . mysqli_real_escape_string($connection, $_GET['siren']) . '", filesize = "' . strlen($result) . '"');
?>