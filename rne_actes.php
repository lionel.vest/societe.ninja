<?php
include('config.php');

$curl = curl_init();

curl_setopt($curl, CURLOPT_URL, "https://registre-national-entreprises.inpi.fr/api/companies/" . $_GET['siren'] . "/attachments");
//curl_setopt($curl, CURLOPT_POSTFIELDS, "grant_type=client_credentials&client_id=" . $piste_client_id . "&client_secret=" . $piste_secret . "&scope=openid");
curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . file_get_contents('private/inpi')));
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HEADER, 0);
$result = curl_exec($curl);
echo $result;
?>