<?php
include('config.php');

$_GET['type'] = $_GET['type'] ? $_GET['type'] : 'users';
if ($_GET['type'] != 'users' AND $_GET['type'] != 'imrs' AND $_GET['type'] != 'filesize')
	die('invalid type');

if (!$_GET['start'])
	$_GET['start'] = date('Y-m-d',strtotime('-30 days'));

if (!$_GET['period'])
	$_GET['period'] = 'day';

if ($_GET['period'] == 'day')
	$period = "DATE_FORMAT(`date`,'%Y-%m-%d')";
else if ($_GET['period'] == 'week')
	$period = "INSERT(YEARWEEK(`date`),5,0,'-')";
else if ($_GET['period'] == 'month')
	$period = "DATE_FORMAT(`date`,'%Y-%m')";
else if ($_GET['period'] == 'year')
	$period = "DATE_FORMAT(`date`,'%Y')";

$query = mysqli_query($connection, "SELECT SUM(" . $_GET['type'] . ") as data, " . $period . " as label FROM stats WHERE `date` >= '" . $_GET['start'] . "' GROUP BY label ORDER BY label DESC LIMIT 0,30");

while($results = mysqli_fetch_array($query, MYSQLI_ASSOC))
{
	if ($_GET['type'] == 'filesize')
		$results['data'] = round($results['data'] / 1024 / 1024);
	$data[] = $results['data'];
	$labels[] = $results['label'];
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
	<title>SOCIETE NINJA</title>
	<meta name="viewport" content="width=device-width, initial-scale=0.9">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="description" content="Accès gratuit aux informations sur les entreprises et sociétés françaises (statuts, PV, procès verbaux, comptes annuels, bilans...)" />
	<link rel="stylesheet" href="/index.css">
	<link rel="icon" type="image/png" sizes="32x32" href="/manifest/favicon.png" />
	<link rel="apple-touch-icon" sizes="180x180" href="/manifest/apple-touch-icon.png">
	<link rel="manifest" href="manifest.json">
	<link rel="mask-icon" href="/manifest/safari-pinned-tab.svg" color="#2d333a">
	<link rel="shortcut icon" href="/manifest/favicon.png">
	<meta name="apple-mobile-web-app-title" content="societe.ninja">
	<meta name="application-name" content="societe.ninja">
	<meta name="msapplication-TileColor" content="#2d333a">
	<meta name="msapplication-config" content="/manifest/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<script src="/app.js"></script>
	<script src="/serviceworker_updater.js" defer></script>
	<script src='https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js'></script>
</head>

<body style="overflow:none">
	<table style="margin:0;box-shadow:unset;border:unset">
		<thead>
			<tr>
				<td colspan="2">
					<select id="type" style="margin:0;color:#000000;width:300px" onchange="search()">
						<option value="users">UTILISATEURS UNIQUES</option>
						<option value="imrs">FICHES CONSULTEES</option>
						<option value="filesize">VOLUME TELECHARGE (en Go)</option>
					</select>
					depuis le 
					<input id="start" type="date" style="margin:0" onchange="search()">
					par
					<select id="period" style="margin:0;color:#000000;width:150px" onchange="search()">
						<option value="day">JOURS</option>
						<option value="week">SEMAINES</option>
						<option value="month">MOIS</option>
						<option value="year">ANNEE</option>
					</select>
				</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="border:0">
					<table id="datatable" style="border:1px solid #000000;border-collapse:collapse;white-space:nowrap;margin:0" border="1" cellpadding="2" cellspacing="0">
						<thead>
							<tr>
								<td style="text-align:center">PERIODE</td>
								<td style="text-align:center">#</td>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</td>
				<td style="width:100%;max-height:800px;border:0">
					<canvas id="chart"></canvas>
				</td>
			</tr>
		</tbody>
	</table>
</body>

<script>
	const query = new URLSearchParams(window.location.search);
	document.getElementById('type').value = query.get('type') || 'users'
	document.getElementById('start').value = query.get('start') || '<?=$_GET['start']?>'
	document.getElementById('period').value = query.get('period') || 'day'

	function search()
	{
		window.location.href = '/stats.php?type=' + document.getElementById('type').value + '&start=' + document.getElementById('start').value + '&period=' + document.getElementById('period').value
	}

	json_data = <?= json_encode($data) ?>;
	json_labels = <?= json_encode($labels) ?>;
	
	for (i=0; i<json_data.length; i++)
	{
		row = document.getElementById('datatable').tBodies[0].insertRow()
		td1 = row.insertCell()
		td1.style.padding = '2px 6px 2px 6px'
		td1.style.textAlign = 'center'
		td1.innerText = json_labels[i];
		td2 = row.insertCell()
		td2.style.padding = '2px 6px 2px 6px'
		td2.style.textAlign = 'right'
		td2.innerText = json_data[i]
	}
	
	
	const ctx = document.getElementById('chart').getContext('2d');
		const chart = new Chart(ctx, {
			type: 'bar',
			data: 
			{
				labels: json_labels.reverse(),
				datasets: 
				[
					{
						data: json_data.reverse(),
						borderColor: 'rgb(75, 192, 192)',
						backgroundColor: 'rgb(75, 192, 192, 0.1)',
						borderWidth: 1,
						order: 1
					},
					{
						data: json_data,
						borderColor: 'rgb(128, 128, 255)',
						backgroundColor: 'rgba(0, 0, 0, 0)',
						tension: 0.1,
						borderWidth: 2,
						tension: 0,
						type: 'line',
						order: 0
					}, 
				]
			},
			options: 
			{
				legend: 
				{
					display: false,
				}
			},
		});
</script>