function rne_actes(siren)
{
	let info =
	{
		"siren": siren, "source": "Registre National des Entreprises (Actes)", "sourcelink": "https://registre-national-entreprises.inpi.fr/api/sso/login", "retry": () => rne_actes(siren)
	}
	add_source(info.source, info.sourcelink, 21)

	fetch('rne_actes.php?siren=' + siren)
		.then(function (response) 
		{
			if (response.ok)
				return response.json()
			else
				throw response
		})
		.then(function (response)
		{
			response = Object.assign(response, info)

			if (response.code == 429)
				return update_source(response.source, 'unavailable', '429', "Quota dépassé", response.retry)

			if (!response.actes || response.actes.length == 0)
				return update_source(response.source, 'validate')

			//ACTES
			document.getElementById('menu_actes').style.display = ''
			document.getElementById('actes').style.display = ''
			let depots = []


			for (acte of response.actes)
			{
				let uniqid = acte.numChrono ? acte.numChrono.split('/').map(part => parseInt(part.replace(/\D/g, ''))).join('') : acte.updatedAt.slice(0, 10).replaceAll('-', '')
				//let uniqid = acte.id
				if (!depots[uniqid])
					depots[uniqid] = { numero: acte.numChrono, dateDepot: new Date(acte.dateDepot), actes: [acte] }
				else
					depots[uniqid].actes.push(acte)
			}

			depots = depots.filter(Boolean)
			depots.sort((a, b) => b.dateDepot - a.dateDepot)

			nb_actes = 0
			for (let depot of depots)
				actes_addrow(siren, depot, response.source)
			document.getElementById('actes').querySelector('thead td').innerHTML = 'ACTES - <i> (' + nb_actes + ' actes  dans ' + depots.length + ' dépôts)</i>'

			//ACTES RBE
			// document.getElementById('menu_actes_rbe').style.display = ''
			// document.getElementById('actes_rbe').style.display = ''
			// let actes_rbe = []
			// for (acte of response.actesRbe)
			// {
			// 	if (!actes_rbe[acte.numChrono])
			// 		actes_rbe[acte.numChrono] = { numero: acte.numChrono, dateDepot: new Date(acte.dateDepot), actes: [acte] }
			// 	else
			// 		actes_rbe[acte.numChrono].actes.push(acte)
			// }

			// actes_rbe = actes_rbe.filter(Boolean)
			// actes_rbe.sort((a, b) => b.dateDepot - a.dateDepot)

			// for (let depot of actes_rbe)
			// 	actes_rbe_addrow(siren, depot, response.source)

			//COMPTES SOCIAUX
			document.getElementById('menu_comptes').style.display = ''
			document.getElementById('comptes').style.display = ''
			if (!response.bilans || response?.bilans?.length == 0)
				document.getElementById('comptes').querySelector('thead td').innerHTML = 'COMPTES PUBLIÉS - <i> Aucun résultat</i>'

			if (response.bilans)
				for (let compte of response.bilans)
				{
					for (let compte_json of response.bilansSaisis)
						if (compte.dateCloture == compte_json.dateCloture)
							compte.json_id = compte_json.id
					comptes_addrow(siren, compte, response.source)
				}


			update_source(response.source, 'validate', response)
		})
		.catch(function (response)
		{
			response = Object.assign(response, info)
			if (response.toString().substring(0, 9) == 'TypeError')
				return update_source(response.source, 'refresh', '502', response, response.retry)
			else if (response.status == 400)
				return update_source(response.source, 'refresh', '400', "Requête mal formatée", response.retry)
			else if (response.status == 401)
				return update_source(response.source, 'refresh', '401', "Accès non autorisé", response.retry)
			else if (response.status == 404)
				return update_source(response.source, 'validate')
			else if (response.status == 403)
				return update_source(response.source, 'refresh', '403', "Accès refusé", response.retry)
			else if (response.status == 429)
				return update_source(response.source, 'refresh', '429', "Quota dépassé", response.retry)
			else if (response.status)
				return update_source(response.source, 'refresh', response.status, response.status + " " + response.statusText, response.retry)
			else
				return update_source(response.source, 'refresh', '???', response, response.retry)
		})
}

function actes_addrow(siren, depot, source)
{
	if (document.getElementById('depot_' + depot.numero))
		document.getElementById('depot_' + depot.numero).remove()

	row = document.getElementById('actes').tBodies[0].insertRow()
	row.id = 'depot' + depot.numero
	row.title = "Source : " + source
	row.style.cursor = 'help'

	td0 = row.insertCell()
	td0.style.textAlign = 'center'
	td0.className = 'print_hide'
	td0.style.cursor = 'pointer'
	td0.style.whiteSpace = 'nowrap'
	td0.innerHTML += '<span>Dépôt ' + (depot.numero ? 'N°' + depot.numero : '') + '<br/>du ' + depot.dateDepot.toLocaleDateString("fr") + '</span>'

	td1 = row.insertCell()
	td1.style.width = '100%'
	td1.style.padding = 0

	table = document.createElement('table')
	table.className = 'nostyle_table'
	table.style = "margin:0;padding:0;height:100%;width:100%;border:0;background:transparent;box-shadow:0"
	table.style.boxShadow = 0
	td1.appendChild(table)
	for (acte of depot.actes)
	{
		subrow = table.insertRow()
		subrow.style.boxShadow = '0'
		subrow.style.background = 'transparent'

		subtd0 = subrow.insertCell()
		subtd0.title = "Clic gauche pour ouvrir dans le navigateur\nClic droit pour télécharger le fichier"
		subtd0.style.textAlign = "center"
		subtd0.style.minWidth = "64px"
		subtd0.innerHTML = '<img alt="pdf" class="icon print_hide" src="/images/pdf.svg" />'
		// if (!acte.dat_acte)
		// 	acte.dat_acte = acte.dateDepot
		// acte.dat_acte = acte.dat_acte.replace('-', '').replace('-', '').replace('/', '').replace('/', '')
		// if (acte.dat_acte.slice(4, 8) >= 1900 && acte.dat_acte.slice(4, 8) <= 2099)
		// 	subtd0.innerHTML += '<br/>' + acte.dat_acte.slice(0, 2) + '/' + acte.dat_acte.slice(2, 4) + '/' + acte.dat_acte.slice(4, 8)
		// else
		// 	subtd0.innerHTML += '<br/>' + acte.dat_acte.slice(6, 8) + '/' + acte.dat_acte.slice(4, 6) + '/' + acte.dat_acte.slice(0, 4)
		subtd0.id = acte.id
		subtd0.onclick = function () { window.open('rne_acte_download.php?id=' + this.id) }
		subtd0.oncontextmenu = function () { window.open('rne_acte_download.php?id=' + this.id + '&method=inline') }

		subtd2 = subrow.insertCell()
		subtd2.style.padding = 4
		subtd2.style.height = '36px'
		subtd2.style.width = '100%'
		subtd2.style.border = 0
		subtd2.style.background = 'transparent'
		type_array = new Array
		if (acte.typeRdd)
			for (type of acte.typeRdd)
			{
				nb_actes++
				type_array.push('<b>' + type.typeActe + '</b>' + (type.decision ? '<br/>' + type.decision : ''))
			}
		else if (acte.nomDocument)
		{
			nb_actes++
			type_array.push('<b>' + acte.nomDocument.replaceAll('_', ' ') + '</b>')
		}
		subtd2.innerHTML = type_array.join('<br/><br/>')
	}
}

function actes_rbe_addrow(siren, depot, source)
{
	console.log(depot)
	if (document.getElementById('acte_rbe_' + depot.numero))
		document.getElementById('acte_rbe_' + depot.numero).remove()

	row = document.getElementById('actes_rbe').tBodies[0].insertRow()
	row.id = 'depot' + depot.numero
	row.title = "Source : " + source
	row.style.cursor = 'help'

	td0 = row.insertCell()
	td0.style.textAlign = 'center'
	td0.className = 'print_hide'
	td0.style.cursor = 'pointer'
	td0.style.whiteSpace = 'nowrap'
	td0.innerHTML += '<span class="smartphone_hide">Dépôt N°' + depot.numero + '<br/>du ' + depot.dateDepot.toLocaleDateString("fr") + '</span>'
	td0.oncontextmenu = function () { window.location.href = '/download_acte.php?siren=' + siren + '&date_depot=' + actes[0].dateDepot; return false }

	td1 = row.insertCell()
	td1.style.width = '100%'
	td1.style.paddingTop = '10px'
	for (acte of depot.actes)
	{
		img = document.createElement('img')
		img.alt = 'pdf'
		img.classList.add('icon', 'print_hide')
		img.src = "/images/pdf.svg"
		img.style.margin = '10px'
		img.onclick = function () { window.open('rne_acte_download.php?id=' + acte.id) }
		img.oncontextmenu = function () { window.open('rne_acte_download.php?id=' + acte.id + '&method=inline') }
		td1.appendChild(img)
	}
}

function comptes_addrow(siren, compte, source)
{
	if (document.getElementById('compte_' + compte.dateCloture))
		document.getElementById('compte_' + compte.dateCloture).remove()

	row = document.getElementById('comptes').tBodies[0].insertRow()
	row.id = 'compte_' + compte.dateCloture
	row.title = "Source : " + source
	row.style.cursor = 'help'

	td1 = row.insertCell()
	td1.style.textAlign = 'center'
	td1.innerHTML = 'Clôture<br/>' + compte.dateCloture.substring(8, 10) + '/' + compte.dateCloture.substring(5, 7) + '/' + compte.dateCloture.substring(0, 4)

	td2 = row.insertCell()
	td2.style.textAlign = 'center'
	td2.innerHTML = 'Dépôt<br/>' + compte.dateDepot.substring(8, 10) + '/' + compte.dateDepot.substring(5, 7) + '/' + compte.dateDepot.substring(0, 4)

	td3 = row.insertCell()
	td3.style.textAlign = 'center'
	td3.style.width = '100%'
	td3.className = 'print_hide'
	//if (compte.dat_depot >= 20170101)
	//if (compte.confid_indic == 1 || compte?.confid_indic == 'OUI' || compte?.confid_indic == 'oui' || compte?.confid_indic == 'Oui')
	//td3.innerHTML = '<img alt="lock" class="icon" src="/images/lock.svg" onclick="alert(\'Ce document n\\\'est pas disponible au public.\\\n La société a opté pour la confidentialité.\')"/>'
	//else if (compte.confid_cr_indic == 1 || compte?.confid_cr_indic == 'OUI' || compte?.confid_cr_indic == 'oui' || compte?.confid_cr_indic == 'Oui')
	//td3.innerHTML = '<img alt="lock" class="icon" src="/images/lock.svg" onclick="alert(\'Ce document n\\\'est pas disponible au public.\\\n La société a opté pour la confidentialité partielle.\')"/>'
	//else
	//{
	td3.innerHTML = '<div style="display:inline-block;margin:5px"><a href="javascript:window.open(\'/rne_bilan_download.php?id=' + compte.id + '\')" oncontextmenu="window.location.href = \'/rne_bilan_download.php?id=' + compte.id + '&method=inline\';return false"><img alt="pdf" style="width:32px;filter:drop-shadow(2px 2px 2px rgba(80,80,80,.7))" src="/images/pdf.svg"/><span class="smartphone_hide"><br/>Télécharger</span></a></div>'
	if (compte.json_id)
		td3.innerHTML += '<div style="display:inline-block;margin:5px"><a href="javascript:window.open(\'/rne_bilan_saisis_download.php?id=' + compte.json_id + '\')" oncontextmenu="window.location.href = \'/rne_bilan_saisis_download.php?id=' + compte.json_id + '&method=inline\';return false"><img alt="json" style="width:32px;filter:drop-shadow(2px 2px 2px rgba(80,80,80,.7))" src="/images/json.svg"/><span class="smartphone_hide"><br/>Télécharger</span></a></div>'
	//}
	//else
	//td3.innerHTML = '<img alt="unavailable" class="icon" src="/images/unavailable.svg" onclick="alert(\'Les bilans déposés avant le 01/01/2017 ne sont pas disponibles\')" />'
}