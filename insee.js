
async function insee(siren)
{
	x = 0
	const info = { "siren": siren, "source": "INSEE (Entreprises)", "sourcelink": "https://portail-api.insee.fr", "retry": () => insee(siren) }
	add_source(info.source, info.sourcelink, 15)

	if (!get_cookie('2'))
		t = await fetch('t.php')

	fetch('https://api.insee.fr/api-sirene/3.11/siren/' + siren,
		{
			headers:
			{
				'Accept': 'application/json',
				"X-INSEE-Api-Key-Integration": atob(get_cookie('2'))
			}
		})
		.then(function (response) 
		{
			if (response.ok)
				return response.json()
			else
				throw response
		})
		.then(function (response)
		{
			response = Object.assign(response, info)

			if (response.uniteLegale.periodesUniteLegale.length == 0)
				return update_source(response.source, 'validate')

			response.uniteLegale.periodesUniteLegale.sort((a, b) => new Date(b.dateDebut) - new Date(a.dateDebut))

			y = 0
			for (periode of response.uniteLegale.periodesUniteLegale)
			{
				y++
				if (y == response.uniteLegale.periodesUniteLegale.length)
					historique_addrow(periode.dateDebut, ["Création &rarr;", [periode.denominationUniteLegale, periode.nomUniteLegale, periode.nomUsageUniteLegale].filter(Boolean).join(' '), categorie_juridique[periode.categorieJuridiqueUniteLegale]].filter(Boolean).join(' '), response.source)
				if (periode.changementEconomieSocialeSolidaireUniteLegale == 1)
					historique_addrow(periode.dateDebut, periode.economieSocialeSolidaireUniteLegale == 'O' ? "Changement de catégorie &rarr; économie sociale" : "Changement de catégorie &rarr; économie non sociale", response.source)
				if (periode.changementCaractereEmployeurUniteLegale == 1)
					historique_addrow(periode.dateDebut, periode.caractereEmployeurUniteLegale == 'O' ? "Changement de catégorie &rarr; Employeur" : "Changement de catégorie &rarr; Non Employeur", response.source)
				if (periode.changementNicSiegeUniteLegale == 1)
					historique_addrow(periode.dateDebut, 'Transfert de siège  &rarr; <a href="#' + response.siren + periode.nicSiegeUniteLegale + '">' + response.siren + periode.nicSiegeUniteLegale + '</a>', response.source)

				if (periode.changementActivitePrincipaleUniteLegale == 1)
					historique_addrow(periode.dateDebut, "Changement d'activité &rarr; " + periode.activitePrincipaleUniteLegale + ' (' + periode.nomenclatureActivitePrincipaleUniteLegale + ')', response.source)
				if (periode.changementCategorieJuridiqueUniteLegale == 1)
					historique_addrow(periode.dateDebut, "Changement de forme &rarr; " + categorie_juridique[periode.categorieJuridiqueUniteLegale], response.source)
				if (periode.changementDenominationUsuelleUniteLegale == 1)
					historique_addrow(periode.dateDebut, "Changement de dénomination usuelle &rarr; " + periode.denominationUsuelle1UniteLegale, response.source)
				if (periode.changementDenominationUniteLegale == 1)
					historique_addrow(periode.dateDebut, "Changement de dénomination sociale &rarr; " + periode.denominationUniteLegale, response.source)
				if (periode.changementNomUsageUniteLegale == 1)
					historique_addrow(periode.dateDebut, "Changement de nom d'usage &rarr; " + periode.nomUniteLegale, response.source)
				if (periode.changementNomUniteLegale == 1)
					historique_addrow(periode.dateDebut, "Changement de nom de naissance &rarr; " + periode.nomUniteLegale, response.source)
				if (periode.changementEtatAdministratifUniteLegale == 1)
					historique_addrow(periode.dateDebut, periode.etatAdministratifUniteLegale == "A" ? "Réouverture de la société" : "Fermeture ou mise en sommeil de la société", response.source)
			}

			document.getElementById('menu_historique').style.display = ''
			document.getElementById('historique').style.display = ''

			update_source(response.source, 'validate', response)
		})
		.catch(function (response)
		{
			response = Object.assign(response, info)
			if (response.toString().substring(0, 9) == 'TypeError')
				return update_source(response.source, 'refresh', '502', response, response.retry)
			else if (response.status == 401)
				return update_source(response.source, 'refresh', '401', "Accès non autorisé", response.retry)
			else if (response.status == 404)
				return update_source(response.source, 'refresh', '404', "Chemin non trouvé", response.retry)
			else if (response.status == 403)
				return update_source(response.source, 'validate', '403', "Accès refusé\nL'entrepreneur individuel s'est opposé à la diffusion de ses données", response.retry)
			else if (response.status)
				return update_source(response.source, 'refresh', response.status, response.status + " " + response.statusText, response.retry)
			else
				return update_source(response.source, 'refresh', '???', response, response.retry)
		})
}

function historique_addrow(key, value, source)
{
	if (document.getElementById(key))
		document.getElementById(key).remove()

	row = document.getElementById('historique').tBodies[0].insertRow()
	row.id = 'historique_' + key
	row.title = "Source : " + source
	row.style.cursor = 'help'
	row.source = source

	td1 = row.insertCell()
	td1.style.whiteSpace = 'nowrap'
	td1.innerHTML = new Date(key).toLocaleDateString()
	td2 = row.insertCell()

	td2.style.width = '100%'
	td2.style.overflowWrap = 'anywhere'
	td2.innerHTML = value
}