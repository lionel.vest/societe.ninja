CREATE TABLE `bans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `ip` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `execution_time` datetime DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `operation` varchar(64) DEFAULT NULL,
  `detail` varchar(255) DEFAULT NULL,
  `filesize` bigint(20) unsigned DEFAULT NULL,
  `uid` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `stats` (
  `date` date NOT NULL,
  `users` int(11) DEFAULT NULL,
  `imrs` int(11) DEFAULT NULL,
  `filesize` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `rbe_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(3) unsigned DEFAULT 0,
  `firstname` varchar(128) DEFAULT NULL,
  `lastname` varchar(128) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `company_name` varchar(128) DEFAULT NULL,
  `siren` varchar(9) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `zipcode` varchar(5) DEFAULT NULL,
  `city_name` varchar(128) DEFAULT NULL,
  `country` smallint(5) unsigned DEFAULT NULL,
  `profession` varchar(128) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `added` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;