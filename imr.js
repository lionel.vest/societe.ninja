function imr(siren)
{
	const info = { "siren": siren, "source": "INPI (Actes et Comptes sociaux)", "sourcelink": "https://data.inpi.fr", "retry": () => imr(siren) }
	add_source(info.source, info.sourcelink, 20)

	fetch('imr.php?siren=' + siren)
		.then(function (response) 
		{
			if (response.ok)
				return response.json()
			else
				throw response
		})
		.then(function (response)
		{
			response = Object.assign(response, info)
			if (!response.data)
				return update_source(response.source, 'validate')

			document.getElementById('menu_documents').style.display = ''
			document.getElementById('documents').style.display = ''

			documents_addcell('Récapitulatif<br/>INPI', function () { window.open('https://data.inpi.fr/export/companies?format=pdf&ids=["' + response.siren + '"]') })
			documents_addcell('Extrait KBIS<span class="smartphone_hide"/><br/>(payant via infogreffe)</span>', function () { window.open('https://www.infogreffe.fr/entreprise-societe/' + response.siren + '/actes-deposes.html#docsOfficiels') })

			let siege = Object
			if (Array.isArray(response.data.dossier))
			{
				if (response.data.dossier.length == 1)
					siege = response.data.dossier
				else
					for (dossier of response.data.dossier)
						if (dossier.identite.type_inscrip == "P" && (!dossier.identite.dat_rad || dossier.identite.dat_rad == ''))
							siege = dossier
			}
			else
				siege = response.data.dossier

			if (siege['@attributes']?.code_greffe)
				unite_legale_addrow('Greffe', greffe[siege['@attributes'].code_greffe], response.source, 60)
			if (siege?.identite?.identite_PM?.montant_cap)
				unite_legale_addrow('Capital Social', Intl.NumberFormat('FR-fr').format(siege.identite.identite_PM.montant_cap) + ' €', response.source, 40)
			if (siege?.identite?.dat_immat)
				unite_legale_addrow('Immatriculation', siege.identite.dat_immat.slice(6, 8) + '/' + siege.identite.dat_immat.slice(4, 6) + '/' + siege.identite.dat_immat.slice(0, 4), response.source, 50)
			if (siege?.identite?.identite_PM?.dat_cloture_exer)
				unite_legale_addrow('Clôture d\'exercice', siege.identite.identite_PM.dat_cloture_exer, response.source, 75)

			actes = new Array
			beneficiaires = new Array
			comptes_annuels = new Array
			observations = new Array
			representants = new Array
			if (Array.isArray(response.data.dossier))
				for (dossier of response.data.dossier)
				{
					if (Array.isArray(dossier?.beneficiaires?.beneficiaire))
						for (beneficiaire of dossier.beneficiaires.beneficiaire)
							beneficiaires.push(beneficiaire)
					else if (dossier.beneficiaires)
						beneficiaires.push(dossier.beneficiaires?.beneficiaire)

					if (Array.isArray(dossier?.actes?.acte))
						for (acte of dossier.actes.acte)
							actes.push(acte)
					else if (dossier.actes)
						actes.push(dossier.actes?.acte)

					if (Array.isArray(dossier?.comptes_annuels?.compte_annuel))
						for (compte_annuel of dossier.comptes_annuels.compte_annuel)
							comptes_annuels.push(compte_annuel)
					else if (dossier.comptes_annuels)
						comptes_annuels.push(dossier.comptes_annuels?.compte_annuel)

					if (Array.isArray(dossier?.observations?.observation))
						for (observation of dossier.observations.observation)
							observations.push(observation)
					else if (dossier.observations)
						observations.push(dossier.observations?.observation)

					if (dossier == siege)
						if (Array.isArray(dossier?.representants?.representant))
							for (representant of dossier.representants.representant)
								representants.push(representant)
						else if (dossier.representants)
							representants.push(dossier.representants?.representant)
				}
			else
			{
				if (Array.isArray(response.data?.dossier?.beneficiaires?.beneficiaire))
					beneficiaires = response.data?.dossier?.beneficiaires?.beneficiaire
				else if (response.data?.dossier?.beneficiaires?.beneficiaire)
					beneficiaires.push(response.data?.dossier?.beneficiaires?.beneficiaire)

				if (Array.isArray(response.data?.dossier?.actes?.acte))
					actes = response.data?.dossier?.actes?.acte
				else if (response.data?.dossier?.actes?.acte)
					actes.push(response.data?.dossier?.actes?.acte)

				if (Array.isArray(response.data?.dossier?.comptes_annuels?.compte_annuel))
					comptes_annuels = response.data?.dossier?.comptes_annuels?.compte_annuel
				else if (response.data?.dossier?.comptes_annuels?.compte_annuel)
					comptes_annuels.push(response.data?.dossier?.comptes_annuels?.compte_annuel)

				if (Array.isArray(response.data?.dossier?.observations?.observation))
					observations = response.data?.dossier?.observations?.observation
				else if (response.data?.dossier?.observations?.observation)
					observations.push(response.data?.dossier?.observations?.observation)

				if (Array.isArray(response.data?.dossier?.representants?.representant))
					representants = response.data?.dossier?.representants?.representant
				else if (response.data?.dossier?.representants?.representant)
					representants.push(response.data?.dossier?.representants?.representant)
			}

			if (beneficiaires.length > 0)
			{
				document.getElementById('menu_beneficiaires').style.display = ''
				document.getElementById('beneficiaires').style.display = ''
				for (beneficiaire of beneficiaires)
					beneficiaires_addrow(beneficiaire, response.source)
			}

			if (actes.length > 0)
			{
				document.getElementById('menu_actes').style.display = ''
				document.getElementById('actes').style.display = ''
				depots = new Array

				for (acte of actes)
				{
					if (!depots[acte.num_depot])
						depots[acte.num_depot] = new Array
					depots[acte.num_depot].push(acte)
				}

				let numeric_array = new Array()
				for (let depot in depots)
					numeric_array.push(depots[depot])
				numeric_array.sort((a, b) => b[0].dat_depot - a[0].dat_depot)
				for ([key, value] of Object.entries(numeric_array))
					actes_addrow(response.siren, key, value, response.source)

				let derniers_statuts = new Array
				for ([key, value] of Object.entries(numeric_array))
					derniers_statuts.push(value.filter(acte => acte.type.includes('Statuts')))
				derniers_statuts = derniers_statuts.filter(acte => acte.length > 0)[0]

				if (derniers_statuts[0].dat_acte.includes('/'))
					derniers_statuts_date = derniers_statuts[0].dat_acte.slice(6, 8) + '/' + derniers_statuts[0].dat_acte.slice(4, 6) + '/' + derniers_statuts[0].dat_acte.slice(0, 4)
				else
					derniers_statuts_date = derniers_statuts[0].dat_acte.slice(0, 2) + '/' + derniers_statuts[0].dat_acte.slice(2, 4) + '/' + derniers_statuts[0].dat_acte.slice(4, 8)

				if (derniers_statuts)
					unite_legale_addrow('Statuts à jour', '<a title="Clic gauche pour ouvrir dans le navigateur\nClic droit pour télécharger le fichier" onclick="javascript:document.getElementById(\'depot_' + derniers_statuts[0].num_depot + '\').firstChild.click()" oncontextmenu="javascript:document.getElementById(\'depot_' + derniers_statuts[0].num_depot + '\').firstChild.oncontextmenu(); return false">Version à jour au ' + derniers_statuts_date + '</a>', response.source, 120)

				document.getElementById('actes').querySelector('thead td').innerHTML = 'ACTES <i>(' + actes.length + ' actes  dans ' + Object.entries(depots).length + ' dépôts)</i>'

			}

			if (comptes_annuels.length > 0)
			{
				document.getElementById('menu_comptes').style.display = ''
				document.getElementById('comptes').style.display = ''
				comptes_annuels.sort((a, b) => b.dat_depot - a.dat_depot)
				for (compte_annuel of comptes_annuels)
					comptes_addrow(compte_annuel, response.siren, response.source)
			}

			if (observations.length > 0)
			{
				document.getElementById('menu_observations').style.display = ''
				document.getElementById('observations').style.display = ''
				for (i = 0; i < observations.length; i++)
				{
					if (!observations[i].dat_ajout)
						observations[i].dat_ajout = observations[i].date_greffe
					if (observations[i].dat_ajout.length == 8)
						observations[i].dat_ajout = observations[i].dat_ajout.substr(6, 2) + '/' + observations[i].dat_ajout.substr(4, 2) + '/' + observations[i].dat_ajout.substr(0, 4)
					observations[i].dat_ajout = new Date(observations[i].dat_ajout)
				}
				observations.sort((a, b) => b.dat_ajout - a.dat_ajout)
				for (observation of observations)
					observations_addrow(observation.num, observation.dat_ajout.toLocaleDateString(), observation.texte, response.source)
			}

			if (representants.length > 0)
			{
				document.getElementById('menu_representants').style.display = ''
				document.getElementById('representants').style.display = ''
				for (representant of representants)
				{
					representant.infos = new Array()
					if (representant.libelle_evt == 'TITMC')
						representant.infos.push(representant.raison_nom)
					else if (representant.type == 'P.Physique')
						representant.infos.push([representant.prenoms, representant.nom_usage, representant.nom_patronymique].filter(Boolean).join(' '))
					else
						representant.infos.push(representant.denomination + (representant.siren ? ' (<a href="/data.html?siren=' + representant.siren + '">' + representant.siren + '</a>)' : ''))
					if (representant.dat_naiss) representant.infos.push("Né(e) le " + representant.dat_naiss.slice(6, 8) + '/' + representant.dat_naiss.slice(4, 6) + '/' + representant.dat_naiss.slice(0, 4) + ' à ' + representant.lieu_naiss + (representant.pays_naiss ? representant.pays_naiss : ''))
					if (representant.adr_rep_1) representant.infos.push(representant.adr_rep_1)
					if (representant.adr_rep_2) representant.infos.push(representant.adr_rep_2)
					if (representant.adr_rep_3) representant.infos.push(representant.adr_rep_3)
					if (representant.adr_rep_cp || representant.adr_rep_ville) representant.infos.push((representant.adr_rep_cp ? representant.adr_rep_cp : '') + ' ' + (representant.adr_rep_ville ? representant.adr_rep_ville : ''))
					if (representant.adr_rep_pays && representant.adr_rep_pays.toUpperCase() != 'FRANCE') representant.infos.push(representant.adr_rep_pays)
					representants_addrow(representant.id_representant, representant.type, representant.qualites, representant.infos, response.source)
				}
			}

			update_source(response.source, 'validate', response)
		})
		.catch(function (response)
		{
			response = Object.assign(response, info)
			if (response.toString().substring(0, 9) == 'TypeError')
				return update_source(response.source, 'refresh', '502', response, response.retry)
			else if (response.status == 400)
				return update_source(response.source, 'refresh', '400', "Requête mal formatée", response.retry)
			else if (response.status == 401)
				return update_source(response.source, 'refresh', '401', "Accès non autorisé", response.retry)
			else if (response.status == 404)
				return update_source(response.source, 'refresh', '404', "Chemin non trouvé", response.retry)
			else if (response.status == 403)
				return update_source(response.source, 'refresh', '403', "Accès refusé", response.retry)
			else if (response.status)
				return update_source(response.source, 'refresh', response.status, response.status + " " + response.statusText, response.retry)
			else
				return update_source(response.source, 'refresh', '???', response, response.retry)
		})
}

function beneficiaires_addrow(beneficiaire, source)
{
	if (document.getElementById('beneficiaire_' + beneficiaire.nom_usage + '_' + beneficiaire.prenoms))
		document.getElementById('beneficiaire_' + beneficiaire.nom_usage + '_' + beneficiaire.prenoms).remove()

	row = document.getElementById('beneficiaires').tBodies[0].insertRow()
	row.id = 'beneficiaire_' + beneficiaire.nom_usage + '_' + beneficiaire.prenoms
	row.title = "Source : " + source
	row.style.cursor = 'help'

	td1 = row.insertCell()
	td1.style.whiteSpace = 'nowrap'
	td1.innerHTML = '<b>' + [beneficiaire.prenoms, beneficiaire.nom_naissance != beneficiaire.nom_usage ? beneficiaire.nom_usage : '', beneficiaire.nom_usage && beneficiaire.nom_naissance != beneficiaire.nom_usage ? ' Né(e) ' : '', beneficiaire.nom_naissance].filter(Boolean).join(' ') + '</b><br/>'
	td1.innerHTML += beneficiaire.nationalite ? 'Nationalité : ' + beneficiaire.nationalite + '<br/>' : ''
	td1.innerHTML += 'Né(e) en ' + beneficiaire.date_naissance

	td2 = row.insertCell()
	td2.style.width = '100%'
	if (beneficiaire.date_greffe)
		td2.innerHTML = 'Bénéficiaire selon déclaration reçue au greffe le ' + beneficiaire.date_greffe.substring(6, 8) + '/' + beneficiaire.date_greffe.substring(4, 6) + '/' + beneficiaire.date_greffe.substring(0, 4) + '<br/>'
	else if (beneficiaire.date_integration)
		td2.innerHTML = 'Bénéficiaire selon déclaration reçue au greffe le ' + beneficiaire.date_integration.substring(6, 8) + '/' + beneficiaire.date_integration.substring(4, 6) + '/' + beneficiaire.date_integration.substring(0, 4) + '<br/>'
	if (beneficiaire.detention_part_totale)
		td2.innerHTML += 'Détention ' + (beneficiaire.detention_part_indirecte == 'OUI' ? 'directe' : 'indirecte') + ' de ' + beneficiaire.detention_part_totale + '% du capital' + (beneficiaire.parts_directes_pleine_propr ? ' dont ' + beneficiaire.parts_directes_pleine_propr + '% en pleine propriété' : '') + '<br/>'
	if (beneficiaire.detention_vote_total)
		td2.innerHTML += 'Détention ' + (beneficiaire.detention_vote_indirecte == 'OUI' ? 'directe' : 'indirecte') + ' de ' + beneficiaire.detention_vote_total + '% des droits de vote'
}

function actes_addrow(siren, depot, actes, source)
{
	if (document.getElementById('depot_' + actes[0].num_depot))
		document.getElementById('depot_' + actes[0].num_depot).remove()

	row = document.getElementById('actes').tBodies[0].insertRow()
	row.id = 'depot_' + actes[0].num_depot
	row.title = "Source : " + source
	row.style.cursor = 'help'

	td0 = row.insertCell()
	td0.style.textAlign = 'center'
	td0.className = 'print_hide'
	td0.style.cursor = 'pointer'
	if (actes[0].dat_depot >= 19930101)
	{
		td0.style.color = '#3276dc'
		td0.style.whiteSpace = 'nowrap'
		td0.title = "Clic gauche pour ouvrir dans le navigateur\nClic droit pour télécharger le fichier"

		if (actes.find(acte => acte.type.toLowerCase().search('bénéficiaire effectif') > -1 || acte.type.toLowerCase().search('bénéficiaires effectifs') > -1))
		{
			td0.innerHTML = '<img alt="lock" class="icon" src="/images/lock.svg"/><span class="smartphone_hide"><br/>Dépôt N°' + actes[0].num_depot + '</span>'
			td0.onclick = function () { let rbepass = prompt("L'accès à ce document est restreint\nMerci d'indiquer le code d'accès"); if (rbepass) open('/download_rbe.php?siren=' + siren + '&date_depot=' + actes[0].dat_depot + '&method=inline&rbepass=' + rbepass) }
			td0.oncontextmenu = function () { let rbepass = prompt("L'accès à ce document est restreint\nMerci d'indiquer le code d'accès"); if (rbepass) window.location.href = '/download_rbe.php?siren=' + siren + '&date_depot=' + actes[0].dat_depot + '&rbepass=' + rbepass; return false }
		}
		else
		{
			td0.innerHTML = '<img alt="pdf" class="icon" src="/images/pdf.svg"/><span class="smartphone_hide"><br/>Dépôt N°' + actes[0].num_depot + '<br/>du ' + actes[0].dat_depot.slice(6, 8) + '/' + actes[0].dat_depot.slice(4, 6) + '/' + actes[0].dat_depot.slice(0, 4) + '</span></a>'
			td0.onclick = function () { window.open('/download_acte.php?siren=' + siren + '&date_depot=' + actes[0].dat_depot + '&method=inline') }
			td0.oncontextmenu = function () { window.location.href = '/download_acte.php?siren=' + siren + '&date_depot=' + actes[0].dat_depot; return false }
		}
	}
	else
	{
		td0.innerHTML = '<img alt="unavailable" class="icon" src="/images/unavailable.svg"/>'
		td0.onclick = function () { alert('Les documents déposés avant le 01/01/1993 ne sont pas disponibles en téléchargement') }
	}


	td1 = row.insertCell()
	td1.style.width = '100%'
	td1.style.padding = 0

	table = document.createElement('table')
	table.className = 'nostyle_table'
	table.style = "margin:0;padding:0;height:100%;width:100%;border:0;background:transparent;box-shadow:0"
	table.style.boxShadow = 0
	td1.appendChild(table)
	for (acte of actes)
	{
		subrow = table.insertRow()
		subrow.style.boxShadow = '0'
		subrow.style.background = 'transparent'

		subtd1 = subrow.insertCell()
		subtd1.style.padding = 4
		subtd1.style.border = 0
		subtd1.style.textAlign = 'center'
		subtd1.style.background = 'transparent'
		subtd1.style.boxShadow = '0'
		if (!acte.dat_acte)
			acte.dat_acte = acte.dat_depot
		acte.dat_acte = acte.dat_acte.replace('-', '').replace('-', '').replace('/', '').replace('/', '')
		if (acte.dat_acte.slice(4, 8) >= 1900 && acte.dat_acte.slice(4, 8) <= 2099)
			subtd1.innerHTML = acte.dat_acte.slice(0, 2) + '/' + acte.dat_acte.slice(2, 4) + '/' + acte.dat_acte.slice(4, 8)
		else
			subtd1.innerHTML = acte.dat_acte.slice(6, 8) + '/' + acte.dat_acte.slice(4, 6) + '/' + acte.dat_acte.slice(0, 4)

		subtd2 = subrow.insertCell()
		subtd2.style.padding = 4
		subtd2.style.height = '36px'
		subtd2.style.width = '100%'
		subtd2.style.border = 0
		subtd2.style.background = 'transparent'
		if (acte.type)
			if (acte.type.length == 2)
				if (acte.type == 'AA')
					subtd2.innerHTML = '<b>' + nature_acte[acte.code_nature] + '</b>'
				else
					subtd2.innerHTML = '<b>' + type_acte[acte.type] + '</b>'
			else
				subtd2.innerHTML = '<b>' + acte.type + '</b>'
		subtd2.innerHTML += acte.decision ? '<br/>' + acte.decision : ''
	}
}

function comptes_addrow(compte, siren, source)
{
	if (document.getElementById('compte_' + compte.dat_cloture))
		document.getElementById('compte_' + compte.dat_cloture).remove()

	row = document.getElementById('comptes').tBodies[0].insertRow()
	row.id = 'compte_' + compte.dat_cloture
	row.title = "Source : " + source
	row.style.cursor = 'help'

	td1 = row.insertCell()
	td1.style.textAlign = 'center'
	td1.innerHTML = 'Clôture<br/>' + compte.dat_cloture.substring(6, 8) + '/' + compte.dat_cloture.substring(4, 6) + '/' + compte.dat_cloture.substring(0, 4)

	td2 = row.insertCell()
	td2.style.textAlign = 'center'
	td2.innerHTML = 'Dépôt<br/>' + compte.dat_depot.substring(6, 8) + '/' + compte.dat_depot.substring(4, 6) + '/' + compte.dat_depot.substring(0, 4)

	td3 = row.insertCell()
	td3.style.textAlign = 'center'
	td3.style.width = '100%'
	td3.className = 'print_hide'
	if (compte.dat_depot >= 20170101)
		if (compte.confid_indic == 1 || compte?.confid_indic == 'OUI' || compte?.confid_indic == 'oui' || compte?.confid_indic == 'Oui')
			td3.innerHTML = '<img alt="lock" class="icon" src="/images/lock.svg" onclick="alert(\'Ce document n\\\'est pas disponible au public.\\\n La société a opté pour la confidentialité.\')"/>'
		else if (compte.confid_cr_indic == 1 || compte?.confid_cr_indic == 'OUI' || compte?.confid_cr_indic == 'oui' || compte?.confid_cr_indic == 'Oui')
			td3.innerHTML = '<img alt="lock" class="icon" src="/images/lock.svg" onclick="alert(\'Ce document n\\\'est pas disponible au public.\\\n La société a opté pour la confidentialité partielle.\')"/>'
		else
		{
			td3.innerHTML = '<div style="display:inline-block;margin:5px"><a href="javascript:window.open(\'/download_compte.php?filetype=pdf&siren=' + siren + '&cloture=' + parseInt(compte.dat_cloture) + '&method=inline\')" oncontextmenu="window.location.href = \'/download_compte.php?filetype=pdf&siren=' + siren + '&cloture=' + parseInt(compte.dat_cloture) + '\';return false"><img alt="pdf" style="width:32px;filter:drop-shadow(2px 2px 2px rgba(80,80,80,.7))" src="/images/pdf.svg"/><span class="smartphone_hide"><br/>Télécharger</span></a></div>'
			td3.innerHTML += '<div style="display:inline-block;margin:5px"><a href="javascript:window.open(\'/download_compte.php?filetype=xml&siren=' + siren + '&cloture=' + parseInt(compte.dat_cloture) + '&method=inline\')" oncontextmenu="window.location.href = \'/download_compte.php?filetype=xml&siren=' + siren + '&cloture=' + parseInt(compte.dat_cloture) + '\';return false"><img alt="xml" style="width:32px;filter:drop-shadow(2px 2px 2px rgba(80,80,80,.7))" src="/images/xml.svg"/><span class="smartphone_hide"><br/>Télécharger</span></a></div>'
		}
	else
		td3.innerHTML = '<img alt="unavailable" class="icon" src="/images/unavailable.svg" onclick="alert(\'Les bilans déposés avant le 01/01/2017 ne sont pas disponibles\')" />'
}

function observations_addrow(id, key, value, source) 
{
	if (document.getElementById(id))
		document.getElementById(id).remove()

	row = document.getElementById('observations').tBodies[0].insertRow()
	row.id = id
	row.title = "Source : " + source
	row.style.cursor = 'help'

	td1 = row.insertCell()
	td1.style.whiteSpace = 'nowrap'
	td1.innerHTML = key

	td2 = row.insertCell()
	td2.style.width = '100%'
	td2.style.overflowWrap = 'anywhere'
	td2.innerHTML = value
}

function representants_addrow(id, type, qualites, infos, source)
{
	if (document.getElementById(id))
		document.getElementById(id).remove()

	row = document.getElementById('representants').tBodies[0].insertRow()
	row.id = id
	row.title = "Source : " + source
	row.style.cursor = 'help'

	td1 = row.insertCell()
	td1.style.whiteSpace = 'nowrap'
	td1.innerHTML = '<img class="smartphone_hide" alt="type" style="width:48px;filter:drop-shadow(1px 1px 1px rgba(80,80,80,.7))" src="/images/' + (type == "P.Morale" ? 'corporation' : 'person') + '.svg"/>'

	td2 = row.insertCell()
	td2.style.width = '30%'
	td2.innerHTML = Array.isArray(qualites.qualite) ? qualites.qualite.map(qualite => qualite[0].toUpperCase() + qualite.slice(1).toLowerCase()).join('<br/>') : qualites.qualite
	//td2.innerHTML = typeof qualites === 'object' ? Object.keys(qualites).map(key => qualites[key][0].toUpperCase() + qualites[key].slice(1).toLowerCase()).join('<br/>') : qualites

	td3 = row.insertCell()
	td3.style.width = '70%'
	td3.innerHTML = infos.join('<br/>')
}
