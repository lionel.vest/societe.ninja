function trademarks(siren)
{
	const info = { "siren": siren, "source": "INPI (Marques)", "sourcelink": "https://data.inpi.fr", "retry": () => trademarks(siren) }
	add_source(info.source, info.sourcelink, 30)

	fetch('https://data.inpi.fr/search',
		{
			method: 'POST',
			body:
				JSON.stringify({
					"query":
					{
						"type": "brands",
						"selectedIds": [],
						"sort": "relevance",
						"order": "asc",
						"nbResultsPerPage": "1000",
						"page": "1",
						"filter": {},
						"q": siren
					},
					"aggregations": ["markCurrentStatusCode", "markFeature", "registrationOfficeCode", "classDescriptionDetails.class"]
				})
		})
		.then(function (response) 
		{
			if (response.ok)
				return response.json()
			else
				throw response
		})
		.then(function (response)
		{
			response = Object.assign(response, info)

			if (response.result.hits.hits.length == 0)
				return update_source(response.source, 'validate')

			document.getElementById('menu_trademarks').style.display = ''
			document.getElementById('trademarks').style.display = ''

			for (marque of response.result.hits.hits)
			{
				classes = new Array()
				for (classe of marque._source.classDescriptionDetails)
					if (classe.class != '00')
						classes.push(classe.class)
				trademarks_addrow(marque._source.applicationNumber, marque._source.registrationOfficeCode, marque._source.markWordElement, classes.join(', '), marque._source.applicationDate, marque._source.expiryDate, marque._source.markFeature, response.source)
			}

			update_source(response.source, 'validate', response)
		})
		.catch(function (response)
		{
			response = Object.assign(response, info)
			if (response.toString().substring(0, 9) == 'TypeError')
			{
				trademarks(siren)
				//return update_source(response.source, 'refresh', '502', response, response.retry)
			}
			else if (response.status == 400)
				return update_source(response.source, 'refresh', '400', "Requête mal formatée", response.retry)
			else if (response.status == 401)
				return update_source(response.source, 'refresh', '401', "Accès non autorisé", response.retry)
			else if (response.status == 404)
				return update_source(response.source, 'refresh', '404', "Chemin non trouvé", response.retry)
			else if (response.status == 403)
				return update_source(response.source, 'refresh', '403', "Accès refusé", response.retry)
			else if (response.status)
				return update_source(response.source, 'refresh', response.status, response.status + " " + response.statusText, response.retry)
			else
				return update_source(response.source, 'refresh', '???', response, response.retry)
		})
}

function trademarks_addrow(numero, office, nom, classes, date_depot, date_expiration, type, source)
{
	if (document.getElementById(numero))
		document.getElementById(numero).remove()

	row = document.getElementById('trademarks').tBodies[0].insertRow()
	row.id = numero
	row.title = 'Source : ' + source
	row.style.cursor = 'help'

	td1 = row.insertCell()
	td1.style.textAlign = 'center'
	td1.innerHTML = office

	td2 = row.insertCell()
	td2.style.textAlign = 'center'
	td2.style.width = '70%'
	td2.style.overflowWrap = 'anywhere'
	td2.innerHTML = nom
	if (type != 'Word')
		td2.innerHTML += '<br/><img src="https://data.inpi.fr/image/marques/' + office + numero + '" alt="marque" style="max-height:150px;max-width:250px" onerror="this.onerror=null; this.remove()" />'

	td3 = row.insertCell()
	td3.style.textAlign = 'center'
	td3.style.width = '30%'
	td3.style.overflowWrap = 'anywhere'
	td3.innerHTML = classes

	td4 = row.insertCell()
	td4.style.textAlign = 'center'
	td4.innerHTML = new Date(date_depot).toLocaleDateString()

	td5 = row.insertCell()
	td5.style.textAlign = 'center'
	td5.innerHTML = new Date(date_expiration).toLocaleDateString()

	td6 = row.insertCell()
	td6.style.textAlign = 'center'
	td6.className = 'print_hide'
	td6.innerHTML = '<a href="https://data.inpi.fr/marques/' + office + numero + '" target="_blank"><img alt="html" class="icon" src="/images/html.svg"/></a>'
}