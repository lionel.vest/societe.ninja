
async function acco(siren)
{
	x = 0
	const info = { "siren": siren, "source": "LEGIFRANCE (Accords d'entreprises)", "sourcelink": "https://legifrance.gouv.fr", "retry": () => acco(siren) }
	add_source(info.source, info.sourcelink, 90)

	if (!get_cookie('1'))
		t = await fetch('t.php')

	let etablissements = await fetch('https://api.piste.gouv.fr/dila/legifrance/lf-engine-app/suggest/acco',
		{
			method: 'POST',
			headers:
			{
				"Authorization": "Bearer " + atob(get_cookie('1')),
				"Accept-Encoding": "gzip,deflate",
				"Content-Type": "application/json",
				"Content-Length": JSON.stringify({ searchText: siren }).length,
				"Connection": "Keep-Alive"
			},
			body: JSON.stringify({ searchText: siren })
		})
		.then(function (response) 
		{
			if (response.ok)
				return response.json()
			else
				throw response
		})
		.catch(function (response)
		{
			response = Object.assign(response, info)
			if (response == 'TypeError: Failed to fetch')
				return update_source(response.source, 'refresh', '502', "API indisponible", response.retry)
			else if (response.status == 401)
				return update_source(response.source, 'refresh', '401', "Accès non autorisé", response.retry)
			else if (response.status == 404)
				return update_source(response.source, 'refresh', '404', "Chemin non trouvé", response.retry)
			else if (response.status == 403)
				return update_source(response.source, 'refresh', '403', "Accès refusé", response.retry)
			else if (response.status)
				return update_source(response.source, 'refresh', '500', response.status + " " + response.statusText, response.retry)
			else
				return update_source(response.source, 'refresh', '???', response, response.retry)
		})

	if (Object.keys(etablissements.results.siret_raison_sociale).length == 0)
		return update_source(info.source, 'validate')

	etablissements_avec_accords = new Array()
	for (etablissement of Object.keys(etablissements.results.siret_raison_sociale))
		if (etablissements_avec_accords.indexOf(etablissements.results.siret_raison_sociale[etablissement].siret) == -1)
			etablissements_avec_accords.push(etablissements.results.siret_raison_sociale[etablissement].siret)

	for (etablissement of etablissements_avec_accords)
	{
		data = new Array()
		body = {
			"recherche":
			{
				"filtres":
					[
						{
							"facette": "SIRET_RAISON_SOCIALE",
							"valeurs": [etablissement]
						}
					],
				"pageNumber": 1,
				"pageSize": 100,
				"operateur": "OU",
				"sort": "DATE_DESC",
				"typePagination": "DEFAUT"
			},
			"fond": "ACCO"
		}

		fetch('https://api.piste.gouv.fr/dila/legifrance/lf-engine-app/search',
			{
				method: 'POST',
				headers:
				{
					"Authorization": "Bearer " + atob(get_cookie('1')),
					"Accept-Encoding": "gzip,deflate",
					"Content-Type": "application/json",
					"Content-Length": JSON.stringify(body).length,
					"Connection": "Keep-Alive"
				},
				body: JSON.stringify(body)
			})
			.then(function (response) 
			{
				if (response.ok)
					return response.json()
				else
					throw response
			})
			.then(function (response)
			{
				response = Object.assign(response, info)

				if (response.results.length == 0)
					return update_source(response.source, 'validate')

				response.siret = etablissements_avec_accords[x]
				x++
				data.push(response)

				acco_addrow(response, response.source)

				document.getElementById('menu_acco').style.display = ''
				document.getElementById('acco').style.display = ''

				if (x == etablissements_avec_accords.length)
					update_source(response.source, 'validate', data)
			})
			.catch(function (response)
			{
				response = Object.assign(response, info)
				if (response == 'TypeError: Failed to fetch')
					return update_source(response.source, 'refresh', '502', "API indisponible", response.retry)
				else if (response.status == 400)
					return update_source(response.source, 'refresh', '400', "Requête mal formatée", response.retry)
				else if (response.status == 401)
					return update_source(response.source, 'refresh', '401', "Accès non autorisé", response.retry)
				else if (response.status == 404)
					return update_source(response.source, 'refresh', '404', "Chemin non trouvé", response.retry)
				else if (response.status == 403)
					return update_source(response.source, 'refresh', '403', "Accès refusé", response.retry)
				else if (response.status)
					return update_source(response.source, 'refresh', response.status, response.status + " " + response.statusText, response.retry)
				else
					return update_source(response.source, 'refresh', '???', response, response.retry)
			})
	}

}

async function acco_download(id)
{
	if (!get_cookie('1'))
		t = await fetch('t.php')

	fetch('https://api.piste.gouv.fr/dila/legifrance/lf-engine-app/consult/acco',
		{
			method: 'POST',
			headers:
			{
				"Authorization": "Bearer " + atob(get_cookie('1')),
				"Accept-Encoding": "gzip,deflate",
				"Content-Type": "application/json",
				"Content-Length": JSON.stringify({ "id": id }).length,
				"Connection": "Keep-Alive"
			},
			body: JSON.stringify({ "id": id })
		})
		.then(function (response) 
		{
			if (response.ok)
				return response.json()
			else
				throw response
		})
		.then(function (response)
		{
			fetch("data:application/msword;base64," + response.acco.data)
				.then(response => response.blob())
				.then(blob => 
				{
					var link = document.createElement("a")
					link.href = window.URL.createObjectURL(blob, { type: 'application/msword' })
					link.download = id + '.docx'
					document.body.appendChild(link)
					link.click()
					document.body.removeChild(link)
				})
		})
}

function acco_addrow(etablissement, source)
{
	table = document.getElementById('acco')

	var i = 0
	for (accord of etablissement.results)
	{
		i++
		row = table.tBodies[0].insertRow()
		row.id = accord.reference
		row.title = 'Source : ' + source
		row.style.cursor = 'help'

		td1 = row.insertCell()
		td1.id = etablissement.siret + '_accords'
		td1.style.textAlign = 'center'
		td1.style.cursor = 'pointer'
		td1.style.color = '#0000FF'
		if (i == 1)
			td1.rowSpan = etablissement.results.length
		else
			td1.className = 'smartphone_only'
		td1.innerHTML = etablissement.siret
		td1.onclick = function () { document.getElementById(this.id.substring(0, 14)).scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' }) }

		td2 = row.insertCell()
		td2.style.textAlign = 'center'
		td2.innerHTML = '<span class="smartphone_hide">Signature<br/></span>' + new Date(accord.dateSignature).toLocaleDateString()

		td3 = row.insertCell()
		td3.style.textAlign = 'center'
		td3.innerHTML = '<span class="smartphone_hide">Diffusion<br/></span>' + new Date(accord.dateDiffusion).toLocaleDateString()

		td4 = row.insertCell()
		td4.style.width = '70%'
		titres = new Array()
		for (titre of accord.titles)
			titres.push(titre.title)
		td4.innerHTML = titres.join('<br/>')

		td5 = row.insertCell()
		td5.style.width = '30%'
		td5.innerHTML = accord.themes.join('<br/>')

		td6 = row.insertCell()
		td6.id = accord.titles[0].id
		td6.style.textAlign = 'center'
		td6.style.cursor = 'pointer'
		td6.className = 'print_hide'
		td6.innerHTML = '<img alt="doc" class="icon" src="/images/doc.svg" /><span class="smartphone_hide"><br/>Télécharger</span>'
		td6.onclick = function () { acco_download(this.id) }
	}
}