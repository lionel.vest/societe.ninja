<?php
include('config.php');

$curl = curl_init();

curl_setopt($curl, CURLOPT_URL, "https://oauth.piste.gouv.fr/api/oauth/token");
curl_setopt($curl, CURLOPT_POSTFIELDS, "grant_type=client_credentials&client_id=" . $piste_client_id . "&client_secret=" . $piste_secret . "&scope=openid");
curl_setopt($curl, CURLOPT_HTTPHEADER, array("Accept-Encoding: gzip,deflate", "Content-Type: application/x-www-form-urlencoded", "Content-Length: 140", "Host: oauth.piste.gouv.fr", "Connection: Keep-Alive", "User-Agent: Apache-HttpClient/4.1.1 (java 1.5)"));
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HEADER, 0);
$result = curl_exec($curl);
$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
if ($http_status==200)
	setrawcookie ('1', base64_encode(json_decode($result)->access_token), time()+3600, '','', true, false);

//curl_setopt($curl, CURLOPT_URL, "https://api.insee.fr/token");
//curl_setopt($curl, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
//curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Basic " . $sirene_auth_request_key));
//curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//$result = curl_exec($curl);
//$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
//if ($http_status==200)
setrawcookie ('2', base64_encode($sirene_api_key), time()+43200, '','', true, false);
?>
