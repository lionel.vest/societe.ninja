const cacheName = "societe_ninja"
const version = "2.72"
const assets = [
	"/index.html",
	"/mentions.html",
	"/index.css",
	//"/acco.js",
	//"/bodacc.js",
	"/constants.js",
	//"/cookie.js",
	//"/designs.js",
	//"/idcc.js",
	//"/imr.js",
	//"/inpi.js",
	//"/insee.js",
	//"/patents.js",
	//"/rna.js",
	"rne.js",
	//"/rnm.js",
	//"/sirene.js",
	//"/trademarks.js",
	"/images/back.svg",
	"/images/bars.svg",
	"/images/corporation.svg",
	"/images/cybertron.png",
	"/images/doc.svg",
	"/images/home.svg",
	"/images/html.svg",
	"/images/json.svg",
	"/images/lock.svg",
	"/images/ninja.png",
	"/images/pdf.svg",
	"/images/person.svg",
	"/images/printer.svg",
	"/images/refresh.svg",
	"/images/unavailable.svg",
	"/images/validate.svg",
	"/images/xml.svg",
	"/images/vest.png",
	"/images/winczewski.png",
	"/manifest/android-chrome-192x192.png",
	"/manifest/android-chrome-512x512.png",
	"/manifest/apple-touch-icon.png",
	"/manifest/favicon-16x16.png",
	"/manifest/favicon-32x32.png",
	"/manifest/favicon.ico",
	"/manifest/favicon.png",
	"/manifest/mstile-150x150.png",
	"/manifest/safari-pinned-tab.svg"
]


self.addEventListener('install', (event) =>
{
	self.skipWaiting()

	event.waitUntil((async () =>
	{
		try
		{
			const cache = await caches.open(cacheName)
			const total = assets.length
			let installed = 0

			await Promise.all(assets.map(async (url) =>
			{
				let controller

				try
				{
					controller = new AbortController()
					const { signal } = controller
					const req = new Request(url, { cache: 'reload' })
					const res = await fetch(req, { signal })

					if (res && res.status === 200)
					{
						await cache.put(req, res.clone())
						installed += 1
					} else
					{
						console.info(`unable to fetch ${url} (${res.status})`)
					}
				} catch (e)
				{
					console.info(`unable to fetch ${url}, ${e.message}`)
					controller.abort()
				}
			}))

			if (installed === total)
				console.info(`application successfully installed (${installed}/${total} files added in cache)`)
			else
				console.info(`application partially installed (${installed}/${total} files added in cache)`)
		}
		catch (e)
		{
			console.error(`unable to install application, ${e.message}`)
		}
	})())
})


self.addEventListener('fetch', event =>
{
	event.respondWith((async () =>
	{
		if (event.request.mode === "navigate" && event.request.method === "GET" && registration.waiting && (await clients.matchAll()).length < 2)
		{
			registration.waiting.postMessage('skipWaiting')
			return new Response("", { headers: { "Refresh": "0" } })
		}
		return await caches.match(event.request) || fetch(event.request)
	})())
})


self.addEventListener('activate', (activateEvent) =>
{
	activateEvent.waitUntil(
		caches.keys().then((keyList) =>
		{
			return Promise.all(keyList.map((key) =>
			{
				if ((cacheName).indexOf(key) === -1)
					return caches.delete(key)
			}))
		})
	)
})


self.addEventListener('message', messageEvent =>
{
	if (messageEvent.data === 'skipWaiting')
		return skipWaiting()
})