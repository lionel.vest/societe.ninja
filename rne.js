function rne(siren)
{
	//console.log(role)
	let info =
	{
		"siren": siren,
		"source": "Registre National des Entreprises (IMR)",
		"sourcelink": "https://registre-national-entreprises.inpi.fr/api/sso/login",
		"retry": () => rne(siren)
	}
	add_source(info.source, info.sourcelink, 21)

	fetch('rne.php?siren=' + siren, { headers: { Token: localStorage.getItem('token') } })
		.then(function (response) 
		{
			if (response.ok)
				return response.json()
			else
				throw response
		})
		.then(function (response)
		{
			response = Object.assign(response, info)

			if (response.code == 429)
				return update_source(response.source, 'unavailable', '429', "Quota dépassé", response.retry)

			//INFOS RCS
			document.getElementById('menu_unite_legale').style.display = ''
			document.getElementById('unite_legale').style.display = ''

			unite_legale_addrow('Capital', response.formality.content.personneMorale.identite.description.montantCapital + ' ' + response.formality.content.personneMorale.identite.description?.deviseCapital + (response.formality.content.personneMorale.identite.description?.capitalVariable ? ' (Variable)' : ''), response.source, 75)

			//REPRESENTANTS
			if (response.formality.content.personneMorale.composition)
				if (response.formality.content.personneMorale.composition.pouvoirs.length > 0)
				{
					document.getElementById('menu_representants').style.display = ''
					document.getElementById('representants').style.display = ''
					let representants = new Array()
					for (let representant of response.formality.content.personneMorale.composition.pouvoirs)
					{
						if (representant.typeDePersonne == 'INDIVIDU')
						{
							if (!representants[representant.individu.descriptionPersonne.prenoms.join('') + representant.individu.descriptionPersonne.nom])
								representants[representant.individu.descriptionPersonne.prenoms.join('') + representant.individu.descriptionPersonne.nom] =
								{
									type: 'individu',
									nom: representant.individu.descriptionPersonne.nom.toUpperCase(),
									prenoms: representant.individu.descriptionPersonne.prenoms.map(prenom => prenom.charAt(0).toUpperCase() + prenom.slice(1).toLowerCase()).join(', '),
									naissance: representant.individu.descriptionPersonne.dateDeNaissance,
									adresse: [[representant.individu.adresseDomicile.codePostal, representant.individu.adresseDomicile.commune].filter(Boolean).join(' '), representant.individu.adresseDomicile.pays],
									nationalite: representant.individu.descriptionPersonne.nationalite,
									qualites: [role[representant.individu.descriptionPersonne?.role || representant?.roleEntreprise]]
								}
							else if (representant.individu.descriptionPersonne.role)
								representants[representant.individu.descriptionPersonne.prenoms.join('') + representant.individu.descriptionPersonne.nom].qualites.push(role[representant.individu.descriptionPersonne?.role])
						}
						else 
						{
							if (!representants[representant.entreprise.denomination?.replace('é', 'e').replace('è', 'e').toUpperCase()])
								representants[representant.entreprise.denomination?.replace('é', 'e').replace('è', 'e').toUpperCase()] =
								{
									type: 'entreprise',
									denomination: representant.entreprise.denomination?.replace('é', 'e').replace('è', 'e').toUpperCase(),
									siren: representant.entreprise.siren,
									adresse: [[representant.adresseEntreprise?.numVoie, representant.adresseEntreprise?.typeVoie, representant.adresseEntreprise?.voie].filter(Boolean).join(' '), representant.adresseEntreprise?.complementLocalisation, new Array(representant.adresseEntreprise?.codePostal, representant.adresseEntreprise?.commune).filter(Boolean).join(' '), representant.entreprise.adresseEntreprise?.pays != 'FRANCE' ? representant.entreprise.adresseEntreprise?.pays : null],
									qualites: [role[representant.entreprise.roleEntreprise]]
								}
							else if (representant.entreprise.role)
								representants[representant.entreprise.denomination.replace('é', 'e').replace('è', 'e').toUpperCase()].qualites.push(role[representant.entreprise?.role])
						}
					}
					for (i = 0; i < Object.keys(representants).length; i++)
						representants_addrow('representant_' + i, representants[Object.keys(representants)[i]], response.source)
				}

			//BENEFICIAIRES EFFECTIFS
			document.getElementById('menu_beneficiaires').style.display = ''
			document.getElementById('beneficiaires').style.display = ''

			row = document.getElementById('beneficiaires').tBodies[0].insertRow()

			if (response.formality.content.personneMorale.beneficiairesEffectifs)
			{
				if (response.formality.content.personneMorale.beneficiairesEffectifs.length > 0)
				{
					document.getElementById('menu_beneficiaires').style.display = ''
					document.getElementById('beneficiaires').style.display = ''
					let beneficiaires_effectifs = new Array()
					for (let beneficiaire of response.formality.content.personneMorale.beneficiairesEffectifs.filter(beneficiaire => !beneficiaire.dateFin))
					{
						if (!beneficiaires_effectifs[beneficiaire.beneficiaire.descriptionPersonne.prenoms.join('') + beneficiaire.beneficiaire.descriptionPersonne.nom])
							beneficiaires_effectifs[beneficiaire.beneficiaire.descriptionPersonne.prenoms.join('') + beneficiaire.beneficiaire.descriptionPersonne.nom] =
							{
								nom: beneficiaire.beneficiaire.descriptionPersonne.nom.toUpperCase(),
								prenoms: beneficiaire.beneficiaire.descriptionPersonne.prenoms.map(prenom => prenom.charAt(0).toUpperCase() + prenom.slice(1).toLowerCase()).join(', '),
								date_naissance: beneficiaire.beneficiaire.descriptionPersonne.dateDeNaissance ? 'le ' + beneficiaire.beneficiaire.descriptionPersonne.dateDeNaissance.slice(8, 10) + '/' + beneficiaire.beneficiaire.descriptionPersonne.dateDeNaissance.slice(5, 7) + '/' + beneficiaire.beneficiaire.descriptionPersonne.dateDeNaissance.slice(0, 4) : null,
								lieu_de_naissance: beneficiaire.beneficiaire.descriptionPersonne.lieuDeNaissance ? ' à ' + beneficiaire.beneficiaire.descriptionPersonne.lieuDeNaissance : null,
								pays_naissance: beneficiaire.beneficiaire.descriptionPersonne.paysNaissance ? 'en ' + beneficiaire.beneficiaire.descriptionPersonne.paysNaissance.charAt(0).toUpperCase() + beneficiaire.beneficiaire.descriptionPersonne.paysNaissance.slice(1).toLowerCase() : null,
								adresse: [[beneficiaire.beneficiaire.adresseDomicile?.numVoie, beneficiaire.beneficiaire.adresseDomicile?.typeVoie, beneficiaire.beneficiaire.adresseDomicile?.voie].filter(Boolean).join(' '), [beneficiaire.beneficiaire.adresseDomicile.codePostal, beneficiaire.beneficiaire.adresseDomicile.commune].filter(Boolean).join(' '), beneficiaire.beneficiaire.adresseDomicile?.pays?.toUpperCase() == 'FRANCE' ? null : beneficiaire.beneficiaire.adresseDomicile.pays].filter(Boolean).join('<br/>'),
								nationalite: beneficiaire.beneficiaire.descriptionPersonne.nationalite,
								date_debut: beneficiaire.beneficiaire.descriptionPersonne.dateEffetRoleDeclarant ? beneficiaire.beneficiaire.descriptionPersonne.dateEffetRoleDeclarant.slice(8, 10) + '/' + beneficiaire.beneficiaire.descriptionPersonne.dateEffetRoleDeclarant.slice(5, 7) + '/' + beneficiaire.beneficiaire.descriptionPersonne.dateEffetRoleDeclarant.slice(0, 4) : null,
								date_fin: beneficiaire.dateFin ? beneficiaire.dateFin.slice(8, 10) + '/' + beneficiaire.dateFin.slice(5, 7) + '/' + beneficiaire.dateFin.slice(0, 4) : null,
								date_declaration: beneficiaire.dateDebut ? beneficiaire.dateDebut.slice(8, 10) + '/' + beneficiaire.dateDebut.slice(5, 7) + '/' + beneficiaire.dateDebut.slice(0, 4) : null,
								modalite: beneficiaire.modalite
							}
					}
					for (let beneficiaire of response.formality.content.personneMorale.beneficiairesEffectifs.filter(beneficiaire => beneficiaire.dateFin && !Object.keys(beneficiaires).includes(beneficiaire.beneficiaire.descriptionPersonne.prenoms.join('') + beneficiaire.beneficiaire.descriptionPersonne.nom)))
					{
						if (!beneficiaires_effectifs[beneficiaire.beneficiaire.descriptionPersonne.prenoms.join('') + beneficiaire.beneficiaire.descriptionPersonne.nom])
							beneficiaires_effectifs[beneficiaire.beneficiaire.descriptionPersonne.prenoms.join('') + beneficiaire.beneficiaire.descriptionPersonne.nom] =
							{
								nom: beneficiaire.beneficiaire.descriptionPersonne.nom.toUpperCase(),
								prenoms: beneficiaire.beneficiaire.descriptionPersonne.prenoms.map(prenom => prenom.charAt(0).toUpperCase() + prenom.slice(1).toLowerCase()).join(', '),
								date_naissance: beneficiaire.beneficiaire.descriptionPersonne.dateDeNaissance ? 'le ' + beneficiaire.beneficiaire.descriptionPersonne.dateDeNaissance.slice(8, 10) + '/' + beneficiaire.beneficiaire.descriptionPersonne.dateDeNaissance.slice(5, 7) + '/' + beneficiaire.beneficiaire.descriptionPersonne.dateDeNaissance.slice(0, 4) : null,
								lieu_de_naissance: beneficiaire.beneficiaire.descriptionPersonne.lieuDeNaissance ? ' à ' + beneficiaire.beneficiaire.descriptionPersonne.lieuDeNaissance : null,
								pays_naissance: beneficiaire.beneficiaire.descriptionPersonne.paysNaissance ? 'en ' + beneficiaire.beneficiaire.descriptionPersonne.paysNaissance.charAt(0).toUpperCase() + beneficiaire.beneficiaire.descriptionPersonne.paysNaissance.slice(1).toLowerCase() : null,
								adresse: [[beneficiaire.beneficiaire.adresseDomicile?.numVoie, beneficiaire.beneficiaire.adresseDomicile?.typeVoie, beneficiaire.beneficiaire.adresseDomicile?.voie].filter(Boolean).join(' '), [beneficiaire.beneficiaire.adresseDomicile.codePostal, beneficiaire.beneficiaire.adresseDomicile.commune].filter(Boolean).join(' '), beneficiaire.beneficiaire.adresseDomicile.pays.toUpperCase() == 'FRANCE' ? null : beneficiaire.beneficiaire.adresseDomicile.pays].filter(Boolean).join('<br/>'),
								nationalite: beneficiaire.beneficiaire.descriptionPersonne.nationalite,
								date_debut: beneficiaire.beneficiaire.descriptionPersonne.dateEffetRoleDeclarant ? beneficiaire.beneficiaire.descriptionPersonne.dateEffetRoleDeclarant.slice(8, 10) + '/' + beneficiaire.beneficiaire.descriptionPersonne.dateEffetRoleDeclarant.slice(5, 7) + '/' + beneficiaire.beneficiaire.descriptionPersonne.dateEffetRoleDeclarant.slice(0, 4) : null,
								date_fin: beneficiaire.dateFin ? beneficiaire.dateFin.slice(8, 10) + '/' + beneficiaire.dateFin.slice(5, 7) + '/' + beneficiaire.dateFin.slice(0, 4) : null,
								date_declaration: beneficiaire.dateDebut ? beneficiaire.dateDebut.slice(8, 10) + '/' + beneficiaire.dateDebut.slice(5, 7) + '/' + beneficiaire.dateDebut.slice(0, 4) : null,
								modalite: beneficiaire.modalite
							}
					}
					for (i = 0; i < Object.keys(beneficiaires_effectifs).length; i++)
						beneficiaires_addrow('beneficiaire_' + i, beneficiaires_effectifs[Object.keys(beneficiaires_effectifs)[i]], response.source)
				}
			}
			else if (localStorage.getItem('token'))
			{
				td1 = row.insertCell()
				td1.style.whiteSpace = 'nowrap'
				td1.innerHTML = '<img class="smartphone_hide" alt="type" style="width:48px;filter:drop-shadow(1px 1px 1px rgba(80,80,80,.7))" src="/images/unavailable.svg"/>'

				td2 = row.insertCell()
				td2.innerHTML = 'Données indisponibles ou bénéficiaires non déclarés'
			}
			else
			{
				td1 = row.insertCell()
				td1.style.whiteSpace = 'nowrap'
				td1.innerHTML = '<img class="smartphone_hide" alt="type" style="width:48px;filter:drop-shadow(1px 1px 1px rgba(80,80,80,.7))" src="/images/lock.svg"/>'

				td2 = row.insertCell()
				td2.innerHTML = '<b>L\'Accès au registre des bénéficiaires effectifs est désormais réservé aux professions listées à <a href="https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000042648575/" target="_blank">l\'article L.561-2 du CMF</a >.</b><br/>'
				td2.innerHTML += 'Si vous êtes avocat, notaire, huissier, comptable, journaliste, vous pouvez demander un accès via <a href="https://www.societe.ninja/rbe_registration.html" target="_blank">ce formulaire</a>.<br/>'
				td2.innerHTML += 'Si vous disposez déjà d\'un code d\'accès, veuillez le renseigner ici : <input onkeypress="if (event.key == \'Enter\') this.blur()" onblur="localStorage.setItem(\'token\',this.value); window.location.reload()">'
			}

			//OBSERVATIONS
			if (response.formality.content.personneMorale?.observations?.rcs)
				if (response.formality.content.personneMorale.observations.rcs.length > 0)
				{
					document.getElementById('menu_observations').style.display = ''
					document.getElementById('observations').style.display = ''
					for (observation of response.formality.content.personneMorale.observations.rcs)
						observations_addrow(observation, response.source)
				}

			update_source(response.source, 'validate', response)
		})
		.catch(function (response)
		{
			console.log(response)
			response = Object.assign(response, info)
			if (response.toString().substring(0, 9) == 'TypeError')
				return update_source(response.source, 'refresh', '502', response, response.retry)
			else if (response.status == 400)
				return update_source(response.source, 'refresh', '400', "Requête mal formatée", response.retry)
			else if (response.status == 401)
				return update_source(response.source, 'refresh', '401', "Accès non autorisé", response.retry)
			else if (response.status == 404)
				return update_source(response.source, 'validate')
			else if (response.status == 403)
				return update_source(response.source, 'refresh', '403', "Accès refusé", response.retry)
			else if (response.status == 429)
				return update_source(response.source, 'refresh', '429', "Quota dépassé", response.retry)
			else if (response.status)
				return update_source(response.source, 'refresh', response.status, response.status + " " + response.statusText, response.retry)
			else
				return update_source(response.source, 'refresh', '???', response, response.retry)
		})
}

function representants_addrow(id, representant, source)
{
	if (document.getElementById(id))
		document.getElementById(id).remove()

	row = document.getElementById('representants').tBodies[0].insertRow()
	row.id = id
	row.title = "Source : " + source
	row.style.cursor = 'help'

	td1 = row.insertCell()
	td1.style.whiteSpace = 'nowrap'
	td1.innerHTML = '<img class="smartphone_hide" alt="type" style="width:48px;filter:drop-shadow(1px 1px 1px rgba(80,80,80,.7))" src="/images/' + (representant.type == "individu" ? 'person' : 'corporation') + '.svg"/>'

	td2 = row.insertCell()
	td2.style.width = '40%'
	td2.innerHTML = representant.qualites.filter(Boolean).join('<br/>')
	// td2.innerHTML = Array.isArray(qualites.qualite) ? qualites.qualite.map(qualite => qualite[0].toUpperCase() + qualite.slice(1).toLowerCase()).join('<br/>') : qualites.qualite
	// //td2.innerHTML = typeof qualites === 'object' ? Object.keys(qualites).map(key => qualites[key][0].toUpperCase() + qualites[key].slice(1).toLowerCase()).join('<br/>') : qualites

	td3 = row.insertCell()
	td3.style.width = '60%'
	if (representant.type == "individu")
	{
		td3.innerHTML = '<b>' + [representant.prenoms, representant.nom].filter(Boolean).join(' ') + '</b>'
		if (representant.naissance)
			td3.innerHTML += '<br/>né(e) en ' + [representant.naissance.substring(5, 7) + '/' + representant.naissance.substring(0, 4), representant.ville_naissance].filter(Boolean).join(' à ')
		if (representant.nationalite)
			td3.innerHTML += '<br/>Nationalité ' + representant.nationalite

	}
	else
		td3.innerHTML = '<a href="/data.html?siren=' + representant.siren + '">' + representant.denomination + '</a>'
	td3.innerHTML += '<br/>' + representant.adresse.filter(Boolean).filter(line => line != ' ').join('<br/>')
}



function beneficiaires_addrow(id, beneficiaire, source)
{
	if (document.getElementById(id))
		document.getElementById(id).remove()

	row = document.getElementById('beneficiaires').tBodies[0].insertRow()
	row.id = id
	row.title = "Source : " + source
	row.style.cursor = 'help'
	if (beneficiaire.date_fin)
		row.style.color = '#FF726F'

	td1 = row.insertCell()
	td1.style.whiteSpace = 'nowrap'
	td1.innerHTML = '<b>' + [beneficiaire.prenoms, beneficiaire.nom].filter(Boolean).join(' ') + '</b>'
	if (beneficiaire.date_naissance || beneficiaire.lieu_de_naissance || beneficiaire.pays_naissance)
		td1.innerHTML += '<br/>' + [beneficiaire.date_naissance || beneficiaire.lieu_de_naissance || beneficiaire.pays_naissance ? 'Né(e)' : null, beneficiaire.date_naissance, beneficiaire.lieu_de_naissance, beneficiaire.pays_naissance].filter(Boolean).join(' ')
	if (beneficiaire.adresse)
		td1.innerHTML += '<br/>' + beneficiaire.adresse
	if (beneficiaire.nationalite)
		td1.innerHTML += '<br/>Nationalité : ' + beneficiaire.nationalite

	td2 = row.insertCell()
	td2.style.width = '100%'
	if (beneficiaire.date_fin)
		td2.innerHTML = 'Ancien bénéficiaire du ' + beneficiaire.date_debut + ' au ' + beneficiaire.date_fin
	else if (beneficiaire.date_debut)
		td2.innerHTML = 'Bénéficiaire depuis le ' + beneficiaire.date_debut

	if (!beneficiaire.date_fin && beneficiaire.date_declaration)
		td2.innerHTML += '<br/>Déclaration au greffe du ' + beneficiaire.date_declaration

	if (beneficiaire.modalite)
	{
		if (!beneficiaire.date_fin && beneficiaire.modalite.detentionPartTotale != 0)
			td2.innerHTML += '<br/>Détention ' + (beneficiaire.modalite.detentionPartIndirecte == true ? 'indirecte' : 'directe') + ' de ' + beneficiaire.modalite.detentionPartTotale + '% du capital' + (beneficiaire.modalite.partsDirectesPleinePropriete ? ' dont ' + beneficiaire.modalite.partsDirectesPleinePropriete + '% en pleine propriété' : '')
		if (!beneficiaire.date_fin && beneficiaire.modalite.detentionVoteTotal != 0)
			td2.innerHTML += '<br/>Détention ' + (beneficiaire.modalite.detentionVoteIndirecte == true ? 'indirecte' : 'directe') + ' de ' + beneficiaire.modalite.detentionVoteTotal + '% des droits de vote'

		if (!beneficiaire.date_fin)
			td2.innerHTML += '<div class="print_hide"><i><a onclick="this.nextSibling.style.display=this.nextSibling.style.display==\'none\'?\'\':\'none\'">Voir le détail des modalités de détention</a><div style="display:none">--> ' + Object.entries(beneficiaire.modalite).filter(entry => entry[1] != 0 && entry[1] != false).map(entry => entry[0] + ' : ' + (entry[1] == true ? 'Oui' : entry[1])).join('<br/>--> ') + '</div></i></div>'
	}
}

function observations_addrow(observation, source) 
{
	if (document.getElementById('observation_' + observation.idObservation))
		document.getElementById('observation_' + observation.idObservation).remove()

	row = document.getElementById('observations').tBodies[0].insertRow()
	row.id = 'observation_' + observation.idObservation
	row.title = "Source : " + source
	row.style.cursor = 'help'

	td1 = row.insertCell()
	td1.style.whiteSpace = 'nowrap'
	if (observation.dateAjout)
		td1.innerHTML = observation.dateAjout.slice(8, 10) + '/' + observation.dateAjout.slice(5, 7) + '/' + observation.dateAjout.slice(0, 4)
	else if (observation.dateGreffe)
		td1.innerHTML = observation.dateGreffe.slice(8, 10) + '/' + observation.dateGreffe.slice(5, 7) + '/' + observation.dateGreffe.slice(0, 4)

	td2 = row.insertCell()
	td2.style.width = '100%'
	td2.style.overflowWrap = 'anywhere'
	td2.innerHTML = observation.texte
}

function unite_legale_addrow(key, value, source, rank)
{
	table = document.getElementById('unite_legale')
	if (document.getElementById(key))
		if (document.getElementById(key).source == "SIRENE V3")
			return true
		else
			document.getElementById(key).remove()

	row = table.tBodies[0].insertRow()
	row.id = key
	row.title = "Source : " + source
	row.style.cursor = 'help'
	row.rank = rank
	row.source = source

	td1 = row.insertCell()
	td1.style.whiteSpace = 'nowrap'
	td1.innerHTML = key
	td2 = row.insertCell()

	td2.style.width = '100%'
	td2.style.overflowWrap = 'anywhere'
	td2.innerHTML = value

	rows = Array.prototype.slice.call(table.tBodies[0].rows, 0)
	table.tBodies[0].innerHTML = null
	rows.sort((a, b) => a.rank - b.rank).forEach(tr => table.tBodies[0].appendChild(tr))
}