function bodacc(siren)
{
	const info = { "siren": siren, "source": "BODACC (Annonces légales)", "sourcelink": "https://www.bodacc.fr", "retry": () => bodacc(siren) }
	add_source(info.source, info.sourcelink, 70)

	fetch('https://bodacc-datadila.opendatasoft.com/api/records/1.0/search/?dataset=annonces-commerciales&q=' + siren + '&rows=999')
		.then(function (response) 
		{
			if (response.ok)
				return response.json()
			else
				throw response;
		})
		.then(function (response)
		{
			response = Object.assign(response, info)
			if (response.records == 0)
				return update_source(response.source, 'validate')

			document.getElementById('menu_bodacc').style.display = ''
			document.getElementById('bodacc').style.display = ''

			response.records.sort((a, b) => parseInt(b.fields.dateparution) - parseInt(a.fields.dateparution));
			for (annonce of response.records)
				bodacc_addrow(annonce.fields.id, annonce.fields.dateparution, annonce.fields.tribunal, annonce.fields.familleavis_lib, annonce.fields.id, 'BODACC')

			update_source(response.source, 'validate', response)
		})
		.catch(function (response)
		{
			response = Object.assign(response, info)
			if (response.toString().substring(0, 9) == 'TypeError')
				return update_source(response.source, 'refresh', '502', response, response.retry)
			else if (response.status == 400)
				return update_source(response.source, 'refresh', '400', "Requête mal formatée", response.retry)
			else if (response.status == 401)
				return update_source(response.source, 'refresh', '401', "Accès non autorisé", response.retry)
			else if (response.status == 404)
				return update_source(response.source, 'refresh', '404', "Chemin non trouvé", response.retry)
			else if (response.status == 403)
				return update_source(response.source, 'refresh', '403', "Accès refusé", response.retry)
			else if (response.status)
				return update_source(response.source, 'refresh', response.status, response.status + " " + response.statusText, response.retry)
			else
				return update_source(response.source, 'refresh', '???', response, response.retry)
		})
}

function bodacc_addrow(key, date, greffe, description, annonce_id, source)
{
	if (document.getElementById(key))
		document.getElementById(key).remove()

	row = document.getElementById('bodacc').tBodies[0].insertRow()
	row.id = key
	row.title = 'Source : ' + source
	row.style.cursor = 'help'

	td1 = row.insertCell()
	td1.style.whiteSpace = 'nowrap'
	td1.style.textAlign = 'center'
	td1.innerHTML = new Date(date).toLocaleDateString()

	td2 = row.insertCell()
	td2.style.width = '20%'
	td2.style.textAlign = 'center'
	td2.innerHTML = greffe.toUpperCase().split("DE ").slice(-1).toString().split("D'").slice(-1)

	td3 = row.insertCell()
	td3.style.width = '80%'
	td3.innerHTML = description

	td4 = row.insertCell()
	td4.style.whiteSpace = 'nowrap'
	td4.style.textAlign = 'center'
	td4.className = 'print_hide'
	td4.innerHTML = '<a href="https://www.bodacc.fr/annonce/detail-annonce/' + annonce_id.substring(0, 1) + '/' + annonce_id.substring(1, 9) + '/' + annonce_id.substring(9) + '" target="_blank"><img alt="html" class="icon" src="/images/html.svg" /><span class="smartphone_hide"><br />Afficher</span></a>'

	//td5 = row.insertCell()
	//td5.style.whiteSpace = 'nowrap'
	//td5.style.textAlign = 'center'
	//td5.className = 'print_hide'
	//td5.innerHTML = '<a href="https://www.bodacc.fr/annonce/telecharger/BODACC_' + annonce_id.substring(0,1) + '%5C' + annonce_id.substring(1, 5) + '%5C' + annonce_id.substring(1, 9) + '%5C1%5CBODACC_' + annonce_id.substring(0,1) + '_PDF_Unitaire_' + annonce_id.substring(1, 9) + '_0' + annonce_id.substring(9) + '.pdf" target="_blank"><img alt="html" class="icon" src="/images/pdf.svg" /><span class="smartphone_hide"><br />Afficher</span></a>'
	//OBJECTIF : https://www.bodacc.fr/annonce/telecharger/BODACC_C%5C2020%5C20200150%5C1%5CBODACC_C_PDF_Unitaire_20200150_02897.pdf
	//           https://www.bodacc.fr/annonce/telecharger/BODACC_C%5C2020%5C20200150%5C1%5CBODACC_B_PDF_Unitaire_20200150_02897.pdf
}

