<?php
include('config.php');
error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
	die("Adresse mail invalide");
if ($_POST['siren'] AND !filter_var($_POST['siren'], FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => "/^\d{9}$/"))))
	die("Numéro SIREN invalide");
if (!filter_var($_POST['zipcode'], FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => "/^\d{5}$/"))))
	die("Code postal invalide");
if (!filter_var($_POST['country'], FILTER_VALIDATE_INT) OR !in_array($_POST['country'],array(74)))
	die("Code pays invalide");
if ($_POST['certify_data'] != 'on')
	die("L'utilisateur n'a pas certifié les données");
if ($_POST['accept_terms'] != 'on')
	die("L'utilisateur n'a pas accepté les conditions générales");

$query = mysqli_query($connection, "SELECT * FROM rbe_users WHERE email = '" . mysqli_real_escape_string($connection, $_POST['email']) . "'");
if (mysqli_num_rows($query) > 0)
	die("Il existe déjà un compte utilisateur utilisant cette adresse email");

function random_string()
{
	$characters = '23456789abcdefghjkmnpqrstuvwxyzABCDEFGHIJKMNPQRSTUVWXYZ';
	$randstring = '';
	for ($i = 0; $i < 16; $i++)
		$randstring .= $characters[rand(0, strlen($characters))];
	return $randstring;
}

$token = random_string();
$crypt_token = bin2hex(openssl_encrypt($token, "AES-128-CTR", $encryption_key, '1234567891021121'));

mail("info@cybertron.fr", "Demande d'accès RBE", 
"<br/>Profession: " . $_POST['profession'] . 
"<br/><br/>Nom: " . $_POST['lastname'] . 
"<br/>Prénom: " . $_POST['firstname'] . 
"<br/>Email: " . $_POST['email'] . 
"<br/><br/>Téléphone: " . $_POST['phone'] . 
"<br/><br/>Société: " . $_POST['company_name'] . 
"<br/>Siren: " . $_POST['siren'] . 
"<br/><br/>Adresse: " . $_POST['address'] . 
"<br/>Code Postal: " . $_POST['zipcode'] . 
"<br/>Ville: " . $_POST['city_name'] . 
"<br/>Pays: " . $_POST['country'] . 
"<br/><br/>A certifié les données: " . $_POST['certify_data'] . 
"<br/><br/>A accepté les CG : " . $_POST['accept_terms'] . 
"<br/><br/><a href=\"https://www.societe.ninja/rbe_activation.php?token=" . $crypt_token . "\">lien d'activation</a>"

,"From: " . $_POST['email'] . "\r\nContent-Type: text/html; charset=UTF-8\r\n");

mysqli_query($connection, "INSERT INTO societe_ninja.rbe_users SET 
status = '0',
profession = '" . mysqli_real_escape_string($connection, $_POST['profession']) . "',
firstname = '" . mysqli_real_escape_string($connection, $_POST['firstname']) . "',
lastname = '" . mysqli_real_escape_string($connection, $_POST['lastname']) . "',
email = '" . mysqli_real_escape_string($connection, $_POST['email']) . "',
phone = '" . mysqli_real_escape_string($connection, $_POST['phone']) . "',
company_name = '" . mysqli_real_escape_string($connection, $_POST['company_name']) . "',
siren = '" . mysqli_real_escape_string($connection, $_POST['siren']) . "',
address = '" . mysqli_real_escape_string($connection, $_POST['address']) . "',
zipcode = '" . mysqli_real_escape_string($connection, $_POST['zipcode']) . "',
city_name = '" . mysqli_real_escape_string($connection, $_POST['city_name']) . "',
country = '" . intval($_POST['country']) . "',
token = '" . $token . "',
added = '" . date('Y-m-d H:i:s') . "'"
);

echo "<br/>";
echo "Votre demande a bien été prise en compte.<br/>";
echo "Un courriel vous sera adressé sous 48h maximum.<br/><br/>";
echo '<a href="https://www.societe.ninja">Retour au site</a>';

?>