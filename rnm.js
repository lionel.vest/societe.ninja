function rnm(siren)
{
	const info = {"siren":siren, "source":"RNM (Entreprises artisanales)", "sourcelink":"https://rnm.artisanat.fr","retry":()=>rnm(siren)}
	add_source(info.source, info.sourcelink, 60)

	fetch('https://api-rnm.artisanat.fr/v2/entreprises/' + siren)
	.then(function(response) 
	{
		if (response.ok)
			return response.json()
		else
			throw response
	})
	.then(function (response)
	{
		response = Object.assign(response, info)

		if (!response.ent_id_siren)
			return update_source(response.source, 'validate')

		document.getElementById('menu_rnm').style.display = ''
		document.getElementById('rnm').style.display = ''

		rnm_addrow('Chambre', response.gest_dept + ' (' + response.gest_reg + ')', response.source)
		rnm_addrow('N° de Gestion', response.ent_id_num_gestion, response.source)
		rnm_addrow('Inscription', new Date(response.ent_act_date_immat_rm).toLocaleDateString(), response.source)
		rnm_addrow('Qualification Dirigeant', response.dir_qa_qualification, response.source)
	
		update_source(response.source, 'validate', response)
	})
	.catch(function(response)
	{
		response = Object.assign(response, info)
		if (response.toString().substring(0, 9) == 'TypeError')
			return update_source(response.source, 'refresh', '502', response, response.retry)
		else if (response.status == 400)
			return update_source(response.source, 'refresh', '400', "Requête mal formatée", response.retry)
		else if (response.status == 401)
			return update_source(response.source, 'refresh', '401', "Accès non autorisé", response.retry)
		else if (response.status == 404)
			return update_source(response.source, 'validate')
		else if (response.status == 403)
			return update_source(response.source, 'refresh', '403', "Accès refusé", response.retry)
		else if (response.status)
			return update_source(response.source, 'refresh', response.status, response.status + " " + response.statusText, response.retry)
		else
			return update_source(response.source, 'refresh', '???', response, response.retry)
	})
}

function rnm_addrow(key, value, source)
{
	if (document.getElementById(key))
		document.getElementById(key).remove()
	
	row = document.getElementById('rnm').tBodies[0].insertRow()
	row.id = key
	row.title = 'Source : ' + source
	row.style.cursor = 'help'

	td1 = row.insertCell()
	td1.style.whiteSpace = 'nowrap'
	td1.innerHTML = key
	
	td2 = row.insertCell()
	td2.style.width = '100%'
	td2.style.overflowWrap = 'anywhere'
	td2.innerHTML = value
}