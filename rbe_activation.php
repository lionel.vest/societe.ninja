<?php
include('config.php');

$token = openssl_decrypt(pack("H*",$_GET['token']), "AES-128-CTR", $encryption_key, '1234567891021121');

$query = mysqli_query($connection, "SELECT * FROM rbe_users WHERE token = '" . mysqli_real_escape_string($connection, $token) . "'");
if (mysqli_num_rows($query) == 0)
	die("invalid token");

mysqli_query($connection, 'UPDATE rbe_users SET status = 1 WHERE token = "' . $token . '"');

?>

Bonjour,<br/>
<br/>
En votre qualité de professionnel autorisé, vous avez demandé un accès au registre des bénéficiaires effectifs via notre site www.societe.ninja.<br/>
Nous avons le plaisir de vous annoncer que votre demande a été acceptée !<br/>
<br/>
Pour activer cette fonction, il vous suffit d'entrer le code d'accès suivant lors de votre prochaine recherche sur notre site : <b><?= $token?></b><br/>
<br/>
Ce code est personnel. Ne le partagez pas, y compris au sein de votre organisation.<br/>
<br/>
Nous rappelons que societe.ninja est un site gratuit créé par des bénévoles et édité par l'association à but non lucratif CYBERTRON.<br/>
Aidez nous à financer nos frais de fonctionnement en faisant un don à l'association : <a href="https://www.paypal.com/donate/?hosted_button_id=UPDJJ2ZRDJ34Y">https://www.paypal.com/donate/?hosted_button_id=UPDJJ2ZRDJ34Y</a><br/>
<br/>
En vous souhaitant bonne réception de la présente<br/>
<br/>
Bien cordialement<br/>
<br/>
L'équipe CYBERTRON<br/>
<br/>
<br/>
<a href="https://www.cybertron.fr"><img alt="LOGO CYBERTRON" src="data:image/png;base64,<?php echo base64_encode(file_get_contents('images/cybertron.png'))?>" width="200"/></a>