function inpi(siren)
{
	const info = { "siren": siren, "source": "INPI (Entreprises)", "sourcelink": "https://data.inpi.fr", "retry": () => inpi(siren) }
	add_source(info.source, info.sourcelink, 20)

	fetch('https://data.inpi.fr/search',
		{
			method: 'POST',
			body:
				JSON.stringify({
					"query":
					{
						"type": "companies",
						"selectedIds": [],
						"sort": "relevance",
						"order": "asc",
						"nbResultsPerPage": "1",
						"page": "1",
						"filter": {},
						"q": siren
					},
					"aggregations": []
				})
		})
		.then(function (response) 
		{
			if (response.ok)
				return response.json()
			else
				throw response
		})
		.then(function (response)
		{
			response = Object.assign(response, info)
			if (response.result.hits.hits.length == 0)
				return update_source(response.source, 'validate')

			document.getElementById('menu_unite_legale').style.display = ''
			document.getElementById('unite_legale').style.display = ''

			document.title = siren + ' (' + response.result.hits.hits[0]._source.denominationOuNomPatronymique + ')'
			document.getElementById('company_name').innerHTML = response.result.hits.hits[0]._source.denominationOuNomPatronymique.toUpperCase()

			if (response.result.hits.hits[0]._source.idt_physical_person)
			{
				unite_legale_addrow('Dénomination Sociale', response.result.hits.hits[0]._source.idt_physical_person.prenom + ' ' + response.result.hits.hits[0]._source.idt_physical_person.nom_patronymique, response.source, 10)
				unite_legale_addrow('Forme Juridique', 'Entreprise individuelle', response.source, 30)
				//unite_legale_addrow('Immatriculation', new Date(response.result.hits.hits[0]._source.idt_date_immat).toLocaleDateString(), response.source, 50)
			}
			else
			{
				unite_legale_addrow('Dénomination Sociale', response.result.hits.hits[0]._source.denominationOuNomPatronymique, response.source, 10)
				unite_legale_addrow('Forme Juridique', response.result.hits.hits[0]._source.idt_pm_form_jur, response.source, 30)
				//unite_legale_addrow('Capital Social', Intl.NumberFormat('FR-fr').format(response.result.hits.hits[0]._source.idt_pm_montant_cap) + ' €', response.source, 40)
				//unite_legale_addrow('Immatriculation', new Date(response.result.hits.hits[0]._source.idt_date_immat).toLocaleDateString(), response.source, 50)
				//if (response.result.hits.hits[0]._source.idt_pm_date_cloture_exer)
				//unite_legale_addrow('Clôture d\'exercice', response.result.hits.hits[0]._source.idt_pm_date_cloture_exer, response.source, 75)
			}
			if (response.result.hits.hits[0]._source.etablissements)
			{
				siege = response.result.hits.hits[0]._source.etablissements.filter(etablissement => ["SIE", "SEP", "PRI"].includes(etablissement.type))[0]
				if (siege.nomCommercial)
					unite_legale_addrow("Nom Commercial", siege.nomCommercial, response.source, 25)
				siege = [siege.adr_ets_1, siege.adr_ets_2, siege.adr_ets_3, siege.adr_ets_cp + ' ' + siege.adr_ets_ville, (siege?.adr_ets_pays?.toUpperCase() != 'FRANCE') ? siege.adr_ets_pays : '']
				unite_legale_addrow('Siège Social', siege.filter(Boolean).join('<br/>'), response.source, 20)
			}

			//unite_legale_addrow('Greffe', greffe[response.result.hits.hits[0]._source.code_greffe], response.source, 60)
			unite_legale_addrow('N° SIREN', siren, response.source, 70)


			// if (response.result.hits.hits[0]._source?.observations?.length > 0)
			// {
			// 	let observations = response.result.hits.hits[0]._source.observations
			// 	observations.sort((a, b) => b.date_ajout - a.date_ajout)
			// 	document.getElementById('menu_observations').style.display = ''
			// 	document.getElementById('observations').style.display = ''
			// 	for (observation of observations)
			// 		if (observation.texte)
			// 			observations_addrow(observation.num, new Date(observation.date_ajout ? observation.date_ajout : observation.date_greffe).toLocaleDateString(), observation.texte, response.source)
			// }

			if (response.result.hits.hits[0]._source?.representants?.length > 0)
			{
				document.getElementById('menu_representants').style.display = ''
				document.getElementById('representants').style.display = ''
				for (representant of response.result.hits.hits[0]._source.representants)
				{
					representant.infos = new Array()
					if (representant.libelle_evt == 'TITMC')
						representant.infos.push(representant.raison_nom)
					else if (representant.type == 'P.Physique')
						representant.infos.push([representant.prenoms, representant.nom_usage, representant.nom_patronymique].filter(Boolean).join(' '))
					else
						representant.infos.push(representant.denomination + (representant.siren ? '(<a href="/data.html?siren=' + representant.siren + '">' + representant.siren + '</a>)' : ''))
					if (representant.date_naiss) representant.infos.push("Né(e) le " + new Date(representant.date_naiss).toLocaleDateString() + ' à ' + representant.lieu_naiss + (representant.pays_naiss ? representant.pays_naiss : ''))
					if (representant.adr_rep_1) representant.infos.push(representant.adr_rep_1)
					if (representant.adr_rep_2) representant.infos.push(representant.adr_rep_2)
					if (representant.adr_rep_3) representant.infos.push(representant.adr_rep_3)
					if (representant.adr_rep_cp || representant.adr_rep_ville) representant.infos.push((representant.adr_rep_cp ? representant.adr_rep_cp : '') + ' ' + (representant.adr_rep_ville ? representant.adr_rep_ville : ''))
					if (representant.adr_rep_pays && representant.adr_rep_pays.toUpperCase() != 'FRANCE') representant.infos.push(representant.adr_rep_pays)
					representants_addrow(representant.id_representant, representant.type, representant.qualites, representant.infos, response.source)
				}
			}

			update_source(response.source, 'validate', response)
		})
		.catch(function (response)
		{
			response = Object.assign(response, info)
			if (response.toString().substring(0, 9) == 'TypeError')
				return update_source(response.source, 'refresh', '502', response, response.retry)
			else if (response.status == 400)
				return update_source(response.source, 'refresh', '400', "Requête mal formatée", response.retry)
			else if (response.status == 401)
				return update_source(response.source, 'refresh', '401', "Accès non autorisé", response.retry)
			else if (response.status == 404)
				return update_source(response.source, 'refresh', '404', "Chemin non trouvé", response.retry)
			else if (response.status == 403)
				return update_source(response.source, 'refresh', '403', "Accès refusé", response.retry)
			else if (response.status)
				return update_source(response.source, 'refresh', response.status, response.status + " " + response.statusText, response.retry)
			else
				return update_source(response.source, 'refresh', '???', response.toString().replace("\r", " "), response.retry)
		})
}

function unite_legale_addrow(key, value, source, rank)
{

	table = document.getElementById('unite_legale')
	if (document.getElementById(key))
		if (document.getElementById(key).source == "SIRENE V3")
			return true
		else
			document.getElementById(key).remove()

	row = table.tBodies[0].insertRow()
	row.id = key
	row.title = "Source : " + source
	row.style.cursor = 'help'
	row.rank = rank
	row.source = source

	td1 = row.insertCell()
	td1.style.whiteSpace = 'nowrap'
	td1.innerHTML = key
	td2 = row.insertCell()

	td2.style.width = '100%'
	td2.style.overflowWrap = 'anywhere'
	td2.innerHTML = value

	rows = Array.prototype.slice.call(table.tBodies[0].rows, 0)
	table.tBodies[0].innerHTML = null
	rows.sort((a, b) => a.rank - b.rank).forEach(tr => table.tBodies[0].appendChild(tr))
}

// function observations_addrow(id, key, value, source) 
// {
// 	if (document.getElementById(id))
// 		document.getElementById(id).remove()

// 	row = document.getElementById('observations').tBodies[0].insertRow()
// 	row.id = id
// 	row.title = "Source : " + source
// 	row.style.cursor = 'help'

// 	td1 = row.insertCell()
// 	td1.style.whiteSpace = 'nowrap'
// 	td1.innerHTML = key

// 	td2 = row.insertCell()
// 	td2.style.width = '100%'
// 	td2.style.overflowWrap = 'anywhere'
// 	td2.innerHTML = value
// }

function representants_addrow(id, type, qualites, infos, source)
{
	if (document.getElementById(id))
		document.getElementById(id).remove()

	row = document.getElementById('representants').tBodies[0].insertRow()
	row.id = id
	row.title = "Source : " + source
	row.style.cursor = 'help'

	td1 = row.insertCell()
	td1.style.whiteSpace = 'nowrap'
	td1.innerHTML = '<img class="smartphone_hide" alt="type" style="width:48px;filter:drop-shadow(1px 1px 1px rgba(80,80,80,.7))" src="/images/' + (type == "P.Morale" ? 'corporation' : 'person') + '.svg"/>'

	td2 = row.insertCell()
	td2.style.width = '30%'
	td2.innerHTML = Array.isArray(qualites) ? qualites.map(qualite => qualite[0].toUpperCase() + qualite.slice(1).toLowerCase()).join('<br/>') : qualites

	td3 = row.insertCell()
	td3.style.width = '70%'
	td3.innerHTML = infos.join('<br/>')
}
