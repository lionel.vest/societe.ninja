async function sirene_etablissements(siren)
{
	if (!get_cookie('1'))
		t = await fetch('t.php')

	const info = { "siren": siren, "source": "SIRENE V3 (Etablissements)", "sourcelink": "https://api.insee.fr/entreprises/api_doc/sirene", "retry": () => sirene_etablissements(siren) }
	add_source(info.source, info.sourcelink, 15)

	fetch('https://api.insee.fr/api-sirene/3.11/siret?q=siren:' + siren + '&nombre=999',
		{
			headers: {
				'Accept': 'application/json',
				"X-INSEE-Api-Key-Integration": atob(get_cookie('2'))
			}
		})
		.then(function (response) 
		{
			if (response.ok)
				return response.json()
			else
				throw response
		})
		.then(function (response)
		{
			response = Object.assign(response, info)

			if (!response.etablissements)
				return update_source(response.source, 'validate')
			window.etablissements = response.etablissements
			etablissements = new Array()
			etablissements.push(...response.etablissements.filter(etablissement => etablissement.etablissementSiege == true))
			etablissements.push(...response.etablissements.filter(etablissement => etablissement.etablissementSiege != true).filter(etablissement => etablissement.periodesEtablissement[0].etatAdministratifEtablissement != 'F').sort((a, b) => new Date(b.dateCreationEtablissement) - new Date(a.dateCreationEtablissement)))
			etablissements.push(...response.etablissements.filter(etablissement => etablissement.etablissementSiege != true).filter(etablissement => etablissement.periodesEtablissement[0].etatAdministratifEtablissement == 'F').sort((a, b) => new Date(b.dateCreationEtablissement) - new Date(a.dateCreationEtablissement)))

			if (etablissements.length > 0)
			{
				document.getElementById('menu_etablissements').style.display = ''
				document.getElementById('etablissements').style.display = ''
				for (etablissement of etablissements)
					if (etablissement.statutDiffusionEtablissement == "O")
						etablissements_addrow(etablissement, response.source)
			}
			update_source(response.source, 'validate', response)
		})
		.catch(function (response)
		{
			response = Object.assign(response, info)
			if (response.toString().substring(0, 9) == 'TypeError')
				return update_source(response.source, 'refresh', '502', response, response.retry)
			else if (response.status == 400)
				return update_source(response.source, 'refresh', '400', "Requête mal formatée", response.retry)
			else if (response.status == 401)
				return update_source(response.source, 'refresh', '401', "Accès non autorisé", response.retry)
			else if (response.status == 404)
				return update_source(response.source, 'validate')
			else if (response.status == 403)
				return update_source(response.source, 'refresh', '403', "Accès refusé", response.retry)
			else if (response.status)
				return update_source(response.source, 'refresh', response.status, response.status + " " + response.statusText, response.retry)
			else
				return update_source(response.source, 'refresh', '???', response, response.retry)
		})
}

function readable_address_etablissement(etablissement)
{
	let adresse = etablissement.adresseEtablissement
	ligne1 = [adresse.numeroVoieEtablissement, adresse.indiceRepetitionEtablissement, adresse.typeVoieEtablissement, adresse.libelleVoieEtablissement].filter(Boolean).join(' ')
	ligne4 = [adresse.codeCedexEtablissement, adresse.libelleCedexEtablissement].filter(Boolean).join(' ')
	ligne5 = [adresse.codePostalEtablissement, adresse.libelleCommuneEtablissement, adresse.libelleCommuneEtrangerEtablissement].filter(Boolean).join(' ')
	return [ligne1, adresse.complementAdresseEtablissement, adresse.distributionSpecialeEtablissement, ligne4, ligne5, adresse.libellePaysEtrangerEtablissement]
}

function etablissements_addrow(etablissement, source)
{
	if (document.getElementById(etablissement.siret))
		document.getElementById(etablissement.siret).remove()

	if (etablissement.etablissementSiege == true)
		unite_legale_addrow("Siège Social", readable_address_etablissement(etablissement).filter(Boolean).join('<br/>'), source, 20)

	row = document.getElementById('etablissements').tBodies[0].insertRow()
	row.id = etablissement.siret
	row.title = "Source : " + source
	row.style.cursor = 'help'

	td1 = row.insertCell()
	td1.style.whiteSpace = 'nowrap'
	td1.innerHTML = [etablissement.periodesEtablissement[0].enseigne1Etablissement, etablissement.periodesEtablissement[0].enseigne2Etablissement, etablissement.periodesEtablissement[0].enseigne3Etablissement, etablissement.siret, (etablissement.etablissementSiege == true) ? '(siège)' : '(secondaire)'].filter(Boolean).join('<br/>')

	td2 = row.insertCell()
	td2.style.textAlign = 'center'
	td2.innerHTML = etablissement.periodesEtablissement[0].activitePrincipaleEtablissement

	td3 = row.insertCell()
	td3.style.textAlign = 'center'
	td3.innerHTML = 'Création<br/>' + new Date(etablissement.periodesEtablissement[etablissement.periodesEtablissement.length - 1].dateDebut != '1900-01-01' ? etablissement.periodesEtablissement[etablissement.periodesEtablissement.length - 1].dateDebut : etablissement.periodesEtablissement[etablissement.periodesEtablissement.length - 1].dateDebut).toLocaleDateString()

	td4 = row.insertCell()
	td4.style.textAlign = 'center'
	td4.innerHTML = etablissement.periodesEtablissement[0].etatAdministratifEtablissement == 'F' ? 'Fermeture<br/>' + new Date(etablissement.periodesEtablissement[0].dateDebut).toLocaleDateString() : '&nbsp;'

	td5 = row.insertCell()
	td5.style.width = '100%'
	td5.style.overflowWrap = 'anywhere'
	td5.innerHTML = readable_address_etablissement(etablissement).filter(Boolean).join('<br/>')
}
