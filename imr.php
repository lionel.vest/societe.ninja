<?php
if ($_GET['siren'] && !preg_match('/^[0-9]{9}+$/',$_GET['siren'])) die ('Invalid siren : ' . $_GET['siren']);
$jsessionid = file_get_contents('private/imr');

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, "https://opendata-rncs.inpi.fr/services/diffusion/imrs-saisis/get?listeSirens=" . $_GET['siren']);
curl_setopt($curl, CURLOPT_HTTPHEADER, array('Cookie: JSESSIONID=' . $jsessionid));
curl_setopt($curl, CURLOPT_BINARYTRANSFER, 1);
curl_setopt($curl, CURLOPT_POST, 0);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HEADER, false);
curl_setopt($curl, CURLOPT_TIMEOUT, 20); 
$result = curl_exec($curl);
$response = json_decode($result);
if(curl_errno($curl))
{
	http_response_code(408);
	die(json_encode(array("code" => 408, "message" => "Erreur " . curl_errno($curl) . ' : ' . curl_error($curl))));
}
	
if ($response->globalErrors)
{
	foreach($response->globalErrors as $error)
		$errors[] = $error;
	http_response_code(400);
	die(json_encode(array("code" => 400, "message" => implode(' - ', $errors))));
}
	
//EXTRACTION DU ZIP CONTENANT LE FICHIER XML (le fichier ZIP est traité en mémoire par décomposition de la chaîne binaire)
//Voir la spécification du format PKZIP : https://users.cs.jmu.edu/buchhofp/forensics/formats/pkzip.html
//On élimine le "central directory" qui commence par "\x50\x4b\x01\x02".
//On décompose chaque fichier sachant que la signature de 4 octets commence toujours par "\x50\x4b\x03\x04"
//Le header de chaque fichier fait 30 octets. Moins la signature, reste 26. +le nom du fichier de longueur variable précisée dans le header.
//Le footer (data descriptor) fait quant à lui 12 octets.
//Ce qu'il y a entre le header et le footer est donc le contenu compressé qu'on peut extraire en mémoire avec gzinflate
//quand il y a un zip dans un zip il y a confusion entre la signature du fichier zip et celle du fichier zip dans le zip
$filesectors = explode("\x50\x4b\x01\x02", $result);
$filesectors = $filesectors[0] . "\x50\x4b\x01\x02" . $filesectors[1];
$filesectors = explode("\x50\x4b\x03\x04", $filesectors);
$zip_in_zip = $filesectors[1] . "\x50\x4b\x03\x04" . $filesectors[2];
$filedescription = unpack("vversion/vflag/vmethod/vmodification_time/vmodification_date/Vcrc/Vcompressed_size/Vuncompressed_size/vfilename_length/vextrafield_length", $zip_in_zip);
$zip = gzinflate(substr($zip_in_zip,26+$filedescription['filename_length'],-12));
$filesectors = explode("\x50\x4b\x01\x02", $zip);
$filesectors = explode("\x50\x4b\x03\x04", $filesectors[0]);
array_shift($filesectors);
foreach($filesectors as $filesector)
{
	$filedescription = unpack("vversion/vflag/vmethod/vmodification_time/vmodification_date/Vcrc/Vcompressed_size/Vuncompressed_size/vfilename_length/vextrafield_length", $filesector);
	$filedescription['filename'] = substr($filesector,26,$filedescription['filename_length']);
	if (substr($filedescription['filename'],-3) == 'xml')
		$xml = gzinflate(substr($filesector,26+$filedescription['filename_length'],-12));
}
$imr = simplexml_load_string($xml);

http_response_code(200);
echo json_encode(array("code" => 200, "data" => $imr));

include('config.php');
if ($debug == 1)
	mysqli_query($connection, 'INSERT INTO logs SET execution_time = "' . date('Y-m-d H:i:s') . '", ip = "' . $_SERVER['REMOTE_ADDR'] . '", operation = "imr_by_siren", detail = "' . $_GET['siren'] . '"');
?>