function get_cookie(name) 
{
	var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
	return v ? v[2] : null;
}

function set_cookie(name, value, days, domain) 
{
	var d = new Date;
	d.setTime(d.getTime() + 24*60*60*1000*days);
	document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString() + (domain?';domain='+domain:'');
}

function delete_cookie(name, domain) 
{ 
	set_cookie(name, '', -1, domain);
}
