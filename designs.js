function designs(siren)
{
	const info = { "siren": siren, "source": "INPI (Modèles)", "sourcelink": "https://data.inpi.fr", "retry": () => designs(siren) }
	add_source(info.source, info.sourcelink, 50)

	fetch('https://data.inpi.fr/search',
		{
			method: 'POST',
			body:
				JSON.stringify({
					"query":
					{
						"type": "drawing_models",
						"selectedIds": [],
						"sort": "relevance",
						"order": "asc",
						"nbResultsPerPage": "1000",
						"page": "1",
						"filter": {},
						"q": siren
					},
					"aggregations": ["classifications.classDescription.classNumber", "registrationOfficeCode"]
				})
		})
		.then(function (response) 
		{
			if (response.ok)
				return response.json()
			else
				throw response
		})
		.then(function (response)
		{
			response = Object.assign(response, info)

			if (response.result.hits.hits.length == 0)
				return update_source(response.source, 'validate')

			document.getElementById('menu_designs').style.display = ''
			document.getElementById('designs').style.display = ''

			for (patent of response.result.hits.hits)
				designs_addrow(patent._source.DesignApplicationNumber, patent._source.registrationOfficeCode, patent._source.designReference, patent._source.designTitle[0].text, patent._source.applicationDate, patent._source.expiryDate, response.source)

			update_source(response.source, 'validate', response)
		})
		.catch(function (response)
		{
			response = Object.assign(response, info)
			if (response.toString().substring(0, 9) == 'TypeError')
			{
				designs(siren)
				//return update_source(response.source, 'refresh', '502', response, response.retry)
			}
			else if (response.status == 400)
				return update_source(response.source, 'refresh', '400', "Requête mal formatée", response.retry)
			else if (response.status == 401)
				return update_source(response.source, 'refresh', '401', "Accès non autorisé", response.retry)
			else if (response.status == 404)
				return update_source(response.source, 'refresh', '404', "Chemin non trouvé", response.retry)
			else if (response.status == 403)
				return update_source(response.source, 'refresh', '403', "Accès refusé", response.retry)
			else if (response.status)
				return update_source(response.source, 'refresh', response.status, response.status + " " + response.statusText, response.retry)
			else
				return update_source(response.source, 'refresh', '???', response, response.retry)
		})
}

function designs_addrow(numero, office, reference, nom, date_depot, date_expiration, source)
{
	if (document.getElementById(numero))
		document.getElementById(numero).remove()

	row = document.getElementById('designs').tBodies[0].insertRow()
	row.id = numero
	row.title = 'Source : ' + source
	row.style.cursor = 'help'

	td1 = row.insertCell()
	td1.style.textAlign = 'center'
	td1.innerHTML = office + ' ' + numero

	td2 = row.insertCell()
	td2.style.textAlign = 'center'
	td2.style.width = '100%'
	td2.style.overflowWrap = 'anywhere'
	td2.innerHTML = nom
	td2.innerHTML += '<br/><img src="https://data.inpi.fr/image/dessins_modeles/' + office + numero + '?ref=001" alt="marque" style="max-height:150px;max-width:300px" onerror="this.onerror=null; this.remove()" />'

	td3 = row.insertCell()
	td3.style.textAlign = 'center'
	td3.innerHTML = new Date(date_depot).toLocaleDateString()

	td4 = row.insertCell()
	td4.style.textAlign = 'center'
	td4.innerHTML = date_expiration ? new Date(date_expiration).toLocaleDateString() : ''

	td5 = row.insertCell()
	td5.style.textAlign = 'center'
	td5.className = 'print_hide'
	td5.innerHTML = '<a href="https://data.inpi.fr/dessins_modeles/' + office + numero + '-001" target="_blank"><img alt="html" class="icon" src="/images/html.svg"/></a>'
}
