<?php
include('config.php');

$curl = curl_init();

//GENERATION D'UN NOUVEAU TOKEN INPI TOUTES LES 10MN
if (time() > filemtime(dirname(__FILE__) . '/private/inpi') + 0)
{
	echo "RENOUVELLEMENT DU TOKEN INPI\n";
	$identifiant_id = rand(0, sizeof($inpi_identifiants)-1);
	$body = '{"username": "' . $inpi_identifiants[$identifiant_id]['login'] . '","password": "' . $inpi_identifiants[$identifiant_id]['password'] . '"}';

	curl_setopt($curl, CURLOPT_URL, 'https://registre-national-entreprises.inpi.fr/api/sso/login');
	//curl_setopt($curl, CURLOPT_HTTPHEADER, array("REMOTE_ADDR: 82.64.23.147", "HTTP_X_FORWARDED_FOR: 82.64.23.147", "HTTP_X_REAL_IP: 82.64.23.147", "ORIGIN: 82.64.23.147"));
	curl_setopt($curl, CURLOPT_POSTFIELDS, $body); 
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($curl);
	echo $result;
	
	echo "Code retour : " . $http_status . "\n";
	$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	if ($http_status==200)
	{
		$result = json_decode($result, true);
		file_put_contents(dirname(__FILE__) . '/private/inpi', $result['token']);
		echo "Nouveau token : " . $result['token'] . "\n";
	}
	else
		echo "Code erreur : " . curl_error($curl) . "\n";
}

//GENERATION D'UN NOUVEAU TOKEN RBE TOUTES LES 10MN
if (time() > filemtime(dirname(__FILE__) . '/private/rbe') + 0)
{
	echo "RENOUVELLEMENT DU TOKEN RBE\n";
	$identifiant_id = rand(0, sizeof($rbe_identifiants)-1);
	$body = '{"username": "' . $rbe_identifiants[$identifiant_id]['login'] . '","password": "' . $rbe_identifiants[$identifiant_id]['password'] . '"}';

	curl_setopt($curl, CURLOPT_URL, 'https://registre-national-entreprises.inpi.fr/api/sso/login');
	curl_setopt($curl, CURLOPT_POSTFIELDS, $body); 
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($curl);
	echo $result;
	
	echo "Code retour : " . $http_status . "\n";
	$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	if ($http_status==200)
	{
		$result = json_decode($result, true);
		file_put_contents(dirname(__FILE__) . '/private/rbe', $result['token']);
		echo "Nouveau token : " . $result['token'] . "\n";
	}
	else
		echo "Code erreur : " . curl_error($curl) . "\n";
}

//GENERATION D'UN NOUVEAU TOKEN IMR TOUTES LES 10MN
// if (time() > filemtime(dirname(__FILE__) . '/private/imr') + 600)
// {
// 	echo "RENOUVELLEMENT DU TOKEN IMR\n";
// 	$identifiant_id = rand(0, sizeof($rncs_identifiants)-1);

// 	curl_setopt($curl, CURLOPT_URL, 'https://opendata-rncs.inpi.fr/services/diffusion/login');
// 	curl_setopt($curl, CURLOPT_HTTPHEADER, array('login: ' . $rncs_identifiants[$identifiant_id]['login'], 'password: ' . $rncs_identifiants[$identifiant_id]['password']));
// 	curl_setopt($curl, CURLOPT_POST, 1);
// 	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
// 	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
// 	curl_setopt($curl, CURLOPT_HEADER, true);
// 	$result = curl_exec($curl);

// 	echo "Code retour : " . $http_status . "\n";
// 	$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
// 	if ($http_status==200)
// 	{
// 		$jsessionid = substr($result,strpos($result,'JSESSIONID=')+11,32);
// 		file_put_contents(dirname(__FILE__) . '/private/imr', $jsessionid);
// 		echo "Nouveau token : " . $jsessionid . "\n";
// 	}
// 	else
// 		echo "Code erreur : " . curl_error($curl) . "\n";
// }

//GENERATION D'UN NOUVEAU TOKEN RBE TOUTES LES 10MN
// if (time() > filemtime(dirname(__FILE__) . '/private/rbe') + 600)
// {
// 	echo "RENOUVELLEMENT DU TOKEN RBE\n";
// 	$identifiant_id = rand(0, sizeof($rbe_identifiants)-1);

// 	curl_setopt($curl, CURLOPT_URL, 'https://opendata-rncs.inpi.fr/services/diffusion/login');
// 	curl_setopt($curl, CURLOPT_HTTPHEADER, array('login: ' . $rbe_identifiants[$identifiant_id]['login'], 'password: ' . $rbe_identifiants[$identifiant_id]['password']));
// 	curl_setopt($curl, CURLOPT_POST, 1);
// 	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
// 	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
// 	curl_setopt($curl, CURLOPT_HEADER, true);
// 	$result = curl_exec($curl);

// 	echo "Code retour : " . $http_status . "\n";
// 	$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
// 	if ($http_status==200)
// 	{
// 		$jsessionid = substr($result,strpos($result,'JSESSIONID=')+11,32);
// 		file_put_contents(dirname(__FILE__) . '/private/rbe', $jsessionid);
// 		echo "Nouveau token : " . $jsessionid . "\n";
// 	}
// 	else
// 		echo "Code erreur : " . curl_error($curl) . "\n";
// }

curl_close($curl);

//GENERATION DU FICHIER DE STATISTIQUES TOUTES LES HEURES
if (time() > filemtime(dirname(__FILE__) . '/stats') + 3600)
{
	$nb_actes_query = mysqli_query($connection, "SELECT COUNT(id) FROM logs WHERE operation = 'acte_by_id' OR operation = 'bilan_by_id'");
	$nb_actes = mysqli_fetch_array($nb_actes_query);
	$nb_actes = $nb_actes['COUNT(id)'];

	$nb_imrs_query = mysqli_query($connection, "SELECT COUNT(id) FROM logs WHERE operation = 'imr_by_siren'");
	$nb_imrs = mysqli_fetch_array($nb_imrs_query);
	$nb_imrs = $nb_imrs['COUNT(id)'];

	$volume_actes_query = mysqli_query($connection, "SELECT SUM(filesize) FROM logs");
	$volume_actes = mysqli_fetch_array($volume_actes_query);
	$volume_actes = $volume_actes['SUM(filesize)'];

	$stats = $nb_actes . "-" . $nb_imrs . "-" . $volume_actes;
	file_put_contents(dirname(__FILE__) . '/stats', $stats);
}

//COMPUTATION DES STATISTIQUES QUOTIDIENNES (TOUTES LES 10 MN)
if (in_array(date('i'), ['5','15','25','35','45','55']))
{
	$date = date('Y-m-d',time()-3600);

	$daily_users_query = mysqli_query($connection, "SELECT COUNT(DISTINCT ip) as ip FROM logs WHERE execution_time BETWEEN '" . $date . " 00:00:00' AND '" . $date . " 23:59:59'");
	$daily_users = mysqli_fetch_array($daily_users_query, MYSQLI_ASSOC);

	$daily_imrs_query = mysqli_query($connection, "SELECT COUNT(operation) as operation FROM logs WHERE operation = 'imr_by_siren' AND execution_time BETWEEN '" . $date . " 00:00:00' AND '" . $date . " 23:59:59'");
	$daily_imrs = mysqli_fetch_array($daily_imrs_query, MYSQLI_ASSOC);

	$daily_filesize_query = mysqli_query($connection, "SELECT SUM(filesize) as filesize FROM logs WHERE execution_time BETWEEN '" . $date . " 00:00:00' AND '" . $date . " 23:59:59'");
	$daily_filesize = mysqli_fetch_array($daily_filesize_query, MYSQLI_ASSOC);

	mysqli_query($connection, "REPLACE INTO stats SET `date` = '" . $date . "', users='" . $daily_users['ip'] . "', imrs='" . $daily_imrs['operation'] . "', filesize='" . $daily_filesize['filesize'] . "'");
}

//BANISSEMENT DES IP QUI GENERENT PLUS DE 250 REQUETES PAR HEURE (VERIFICATION TOUTES LES 3MN)
if (in_array(date('i'), ['00','03','06','09','12','15','18','21','24','27','30','33','36','39','42','45','48','51','54','57']))
{
	$spammers_query = mysqli_query($connection, 'SELECT COUNT(operation), ip FROM logs WHERE execution_time > "' . date("Y-m-d H:i:s", strtotime( '-1 hour' )) . '" AND ip NOT IN (SELECT ip FROM bans) GROUP BY ip ORDER BY COUNT(operation) DESC');
	while($spammer = mysqli_fetch_array($spammers_query, MYSQLI_ASSOC))
		if ($spammer['COUNT(operation)'] > 250)
		{
				mysqli_query($connection, "INSERT INTO bans SET ip = '" . $spammer['ip'] . "', `datetime` = '" . date("Y-m-d H:i:s") . "'");
				exec("ufw insert 1 reject from " . $spammer['ip'] . " to any");
		}
}
?>
