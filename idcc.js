function idcc(siren)
{
	const info = { "siren": siren, "source": "IDCC (Conventions Collectives)", "sourcelink": "https://travail-emploi.gouv.fr/", "retry": () => idcc(siren) }
	add_source(info.source, info.sourcelink, 80)

	fetch('https://siret2idcc.fabrique.social.gouv.fr/api/v2/' + siren)
		.then(function (response) 
		{
			if (response.ok)
				return response.json()
			else
				throw response
		})
		.then(function (response)
		{
			response = Object.assign(response, info)

			if (!response[0].conventions[0])
				return update_source(response.source, 'validate')

			document.getElementById('menu_unite_legale').style.display = ''
			document.getElementById('unite_legale').style.display = ''

			conventions = new Array()
			for (convention of response[0].conventions)
				conventions.push('<a href="javascript:window.open(\'' + convention.url + '\')">' + convention.nature + ' ' + convention.num + ' : ' + convention.shortTitle + '</a>')
			unite_legale_addrow('Convention Collective', conventions.join('<br/>'), response.source, 110)

			update_source(response.source, 'validate', response)
		})
		.catch(function (response)
		{
			response = Object.assign(response, info)
			if (response.toString().substring(0, 9) == 'TypeError')
				return update_source(response.source, 'refresh', '502', response, response.retry)
			else if (response.status == 400)
				return update_source(response.source, 'refresh', '400', "Requête mal formatée", response.retry)
			else if (response.status == 401)
				return update_source(response.source, 'refresh', '401', "Accès non autorisé", response.retry)
			else if (response.status == 404)
				return update_source(response.source, 'refresh', '404', "Chemin non trouvé", response.retry)
			else if (response.status == 403)
				return update_source(response.source, 'refresh', '403', "Accès refusé", response.retry)
			else if (response.status)
				return update_source(response.source, 'refresh', response.status, response.status + " " + response.statusText, response.retry)
			else
				return update_source(response.source, 'refresh', '???', response, response.retry)
		})
}